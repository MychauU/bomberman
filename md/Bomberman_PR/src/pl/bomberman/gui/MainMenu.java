/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Grzechu
 */
public class MainMenu extends JPanel{
    
    private static String title = "BomberMan";
    private static int width = 600;
    private static int height = (width / 16) * 9;
    private static int scale = 1;
    private static int buttonsWidth = 150;
    private static int buttonsHeight = 50;
    private static int menuPositionX = (width-buttonsWidth)/2;
    private static int menuPositionY = 108;
    
    private static final long serialVersionUID = 2L;

    private final JMainWindow parent;
    private JButton startButton;
    private JButton exitButton;
    private Image image = null;
    private JLabel reszkaLabel;
    private JLabel majkelLabel;
    private JLabel grzechuLabel;

    MainMenu(JMainWindow parent) {
        this.parent = parent;
        setName("menu");
    }
    
    public void start() {
        setLayout(null);
        setName("main");
        setBackground(Color.WHITE);
        
        startButton = new JButton("Nowa Gra");
        startButton.setBounds(menuPositionX, menuPositionY,buttonsWidth,buttonsHeight);
        startButton.addActionListener((ActionEvent e) -> {
            parent.changePanel(parent.lobby);    
        });
        
        exitButton = new JButton("Wyście");
        exitButton.setBounds(menuPositionX, menuPositionY+buttonsHeight + 10 ,buttonsWidth,buttonsHeight);
        exitButton.addActionListener((ActionEvent e) -> {
            parent.dispose();
            
        });
        
        reszkaLabel = new JLabel("Damian Reszka");
        reszkaLabel.setBounds(7,212,120,25);
        reszkaLabel.setForeground (Color.ORANGE);
        changeFontSize(reszkaLabel);
        
        majkelLabel = new JLabel("Michał Dobreńko");
        majkelLabel.setBounds(5,262,128,25);
        majkelLabel.setForeground (Color.ORANGE);
        changeFontSize(majkelLabel);
        
        grzechuLabel = new JLabel("Grzegorz Rubin");
        grzechuLabel.setBounds(7,237,120,25);
        grzechuLabel.setForeground (Color.ORANGE);
        changeFontSize(grzechuLabel);
        
        
       setBackgroundImage();
        
        add(startButton);
        add(exitButton);
        add(reszkaLabel);
        add(majkelLabel);
        add(grzechuLabel);
        setVisible(true);
    }
    
     @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g); // paint the background image and scale it to fill the entire space
        g.drawImage(image, 0, 0,width,height,null);
        //g.setColor(Color.WHITE);
        //g.fillRect(menuPositionX +10, menuPositionY-10, 260, 130);
    }
    
    protected void setBackgroundImage(){
         try {
             image = ImageIO.read(MainMenu.class.getResourceAsStream("/textures/images/background.png"));
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
    
    private void changeFontSize(JLabel label){
        Font labelFont = label.getFont();
        String labelText = label.getText();

        int stringWidth = label.getFontMetrics(labelFont).stringWidth(labelText);
        int componentWidth = label.getWidth();

        // Find out how much the font can grow in width.
        double widthRatio = (double)componentWidth / (double)stringWidth;

        int newFontSize = (int)(labelFont.getSize() * widthRatio);
        int componentHeight = label.getHeight();

        // Pick a new font size so it will not be larger than the height of label.
        int fontSizeToUse = Math.min(newFontSize, componentHeight);

        // Set the label's font size to the newly determined size.
        label.setFont(new Font(labelFont.getName(), Font.PLAIN, fontSizeToUse));

    }
    
}
