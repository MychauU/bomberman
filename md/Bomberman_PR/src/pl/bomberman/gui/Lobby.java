/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import pl.bomberman.client.Client;
import pl.bomberman.sendpack.MESSAGETYPE;
import pl.bomberman.sendpack.MessagePackage;

/**
 *
 * lobby po nacisnieciu nowa gra
 */
public class Lobby extends JPanel {

    private static String title = "BomberMan";
    private static int width = 600;
    private static int height = (width / 16) * 9;
    private static int scale = 1;
    private static int menuPositionX = 175;
    private static int menuPositionY = 108;
    
    private static final long serialVersionUID = 2L;

    private final JMainWindow parent;
    private JTextField ipTextField;
    private JTextField nameTextField;
    private JLabel ipLabel;
    private JLabel nameLabel;
    private JLabel reszkaLabel;
    private JLabel majkelLabel;
    private JLabel grzechuLabel;
    private JButton startButton;
    private JButton connectButton;
    private JButton wsteczButton;
    private String name;
    private String ip;
    private Image image = null;

    Lobby(JMainWindow parent) {
        this.parent = parent;
    }
    
    public void setStartButton(boolean pom){
        startButton.setEnabled(pom);
    }

    public void setConnectButton(boolean pom){
        connectButton.setEnabled(pom);
    }
    
    public void initializeApp() {
        this.setLayout(null);
        this.setName("lobby");
        this.setBackground(Color.yellow);
        ipTextField = new JTextField("localhost");
        ipTextField.setEditable(true);
        ipTextField.setBounds(menuPositionX+130, menuPositionY, 120, 30);
        nameTextField = new JTextField("annonymous");
        nameTextField.setBounds(menuPositionX+130, menuPositionY+40, 120, 30);
        nameTextField.setEditable(true);
        connectButton = new JButton("Dołącz");
        connectButton.setBounds(menuPositionX+135, menuPositionY+80, 120, 30);
        connectButton.addActionListener((ActionEvent e) -> {
            name = nameTextField.getText();
            ip = ipTextField.getText();
            System.out.println(name + "  " + ip);
            JMainWindow.client.connect(name, ip);
            
        });
        startButton = new JButton("Start");
        startButton.setBounds(menuPositionX+135, menuPositionY+110, 120, 30);
        
        startButton.addActionListener((ActionEvent e) -> {
            Client.messages.add(new MessagePackage(null,MESSAGETYPE.CREATENEWGAME,0));
        });
        
        startButton.setEnabled(false);
        wsteczButton = new JButton("Wstecz");
        wsteczButton.setBounds(menuPositionX+15,menuPositionY+80,120,30);
        wsteczButton.addActionListener((ActionEvent e) -> {
            parent.stopConnection();
            this.setStartButton(false);
            this.setConnectButton(true);
            parent.changePanel(parent.mainMenu);    
            
        });
       
        ipLabel = new JLabel("Adres serwera:  ", SwingConstants.RIGHT);
        ipLabel.setBounds(menuPositionX, menuPositionY, 120, 30);

        nameLabel = new JLabel("Nazwa Gracza:  ", SwingConstants.RIGHT);
        nameLabel.setBounds(menuPositionX, menuPositionY+40, 120, 30);

        reszkaLabel = new JLabel("Damian Reszka");
        reszkaLabel.setBounds(7,212,120,25);
        reszkaLabel.setForeground (Color.ORANGE);
        changeFontSize(reszkaLabel);
        
        majkelLabel = new JLabel("Michał Dobreńko");
        majkelLabel.setBounds(5,262,128,25);
        majkelLabel.setForeground (Color.ORANGE);
        changeFontSize(majkelLabel);
        
        grzechuLabel = new JLabel("Grzegorz Rubin");
        grzechuLabel.setBounds(7,237,120,25);
        grzechuLabel.setForeground (Color.ORANGE);
        changeFontSize(grzechuLabel);
       
        
        add(ipTextField, BorderLayout.LINE_START);
        add(nameTextField);
        add(connectButton);
        add(wsteczButton);
        add(startButton);
        add(ipLabel);
        add(nameLabel);
        add(reszkaLabel);
        add(majkelLabel);
        add(grzechuLabel);
        setBackgroundImage();
        
    }
    
    
    
    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g); // paint the background image and scale it to fill the entire space
        g.drawImage(image, 0, 0,width,height,null);
        g.setColor(Color.WHITE);
        g.fillRect(menuPositionX +10, menuPositionY-10, 260, 160);
    }

    protected void setBackgroundImage(){
         try {
             image = ImageIO.read(getClass().getResourceAsStream("/textures/images/background.png"));
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
    private void changeFontSize(JLabel label){
        Font labelFont = label.getFont();
        String labelText = label.getText();

        int stringWidth = label.getFontMetrics(labelFont).stringWidth(labelText);
        int componentWidth = label.getWidth();

        // Find out how much the font can grow in width.
        double widthRatio = (double)componentWidth / (double)stringWidth;

        int newFontSize = (int)(labelFont.getSize() * widthRatio);
        int componentHeight = label.getHeight();

        // Pick a new font size so it will not be larger than the height of label.
        int fontSizeToUse = Math.min(newFontSize, componentHeight);

        // Set the label's font size to the newly determined size.
        label.setFont(new Font(labelFont.getName(), Font.PLAIN, fontSizeToUse));

    }
}
