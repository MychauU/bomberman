/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.gui;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import pl.bomberman.Game;
import pl.bomberman.client.Client;
/**
 *
 * glowne okno aplikacji przed polaczeniem sie
 */
public class JMainWindow extends JFrame {

    public static Client client;
    private static final String title = "BomberMan";
    private static final int width = 600;
    private static final int height = (width / 16) * 9;
    private static final int scale = 1;
    private static final long serialVersionUID = 2L;
    private Game game;
    protected Lobby lobby;

    public Lobby getLobby() {
        return lobby;
    }

    public void setLobby(Lobby lobby) {
        this.lobby = lobby;
    }
    protected MainMenu mainMenu;
    
    public JMainWindow() {
        
    }
    
    //not used
   // private static final Object lock = new Object();

    public void initializeApp() {
        
        lobby = new Lobby(this);
        lobby.setName("lobby");
        lobby.initializeApp();
        
        mainMenu = new MainMenu(this);
        mainMenu.start();
        
        
        
        game = new Game(this);
        game.setName("game");
        
        client=new Client(this,game);
        
        Dimension size = new Dimension(width * scale, height * scale);
        
        this.setPreferredSize(size);
        this.setResizable(false);
        this.setTitle(title);
        this.setLayout(new BorderLayout());        
        this.mainMenu.start();
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.changePanel(mainMenu);
        this.setVisible(true);

    }

   
    

    public void changePanel(Component panel) {
        getContentPane().removeAll();
        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().doLayout();
        update(getGraphics());
        if ("game".equals(panel.getName())) {
            game.start();
            this.setVisible(false);

        }else if("lobby".equals(panel.getName())){ //dodane
         //   lobbyPanel.initializeApp();
        }else if("menu".equals(panel.getName())){
           // mainMenuPanel.start();
            //mainMenuPanel.startButton.addActionListener(new JMainWindow.MenuAction(lobbyPanel));
        }

    }

    public void setCompAfterLeavingGame() {
        changePanel(lobby);
    }
    
    public void setGameComp(){
        this.changePanel(game);
    }




    private class MenuAction implements ActionListener {

        private final Component comp;

        private MenuAction(Component pnl) {
            this.comp = pnl;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            changePanel(comp);

        }

    }
    
    public void stopConnection(){
        client.stopConnection();
    }
    
}
