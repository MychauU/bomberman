/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.sendpack;

import java.io.Serializable;

/**
 *
 * klasa przechowujaca dane ktore sa paczkowane i ich id do odroznienia i jak je castowac
 */
public class MessagePackage implements Serializable {
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    public Object obj;
    public MESSAGETYPE message;
    public int id;
    public Object additionalInformation;
    public MessagePackage(){
        obj=null;
        message=null;
        additionalInformation=999;
        id=999;
    }
    
    public MessagePackage(Object obj,MESSAGETYPE message,int id){
        this.obj=obj;
        this.message=message;
        this.id=id;
    }
    public MessagePackage(Object obj,MESSAGETYPE message){
        this.obj=obj;
        this.message=message;
        this.id=999;
    }
}
