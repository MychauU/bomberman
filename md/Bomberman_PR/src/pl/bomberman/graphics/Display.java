/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.graphics;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Iterator;
import pl.bomberman.entity.mob.Player;
import pl.bomberman.level.Level;
import pl.bomberman.level.tile.Tile;

/**
 *
 *  klasa kopiujaca piksele i piszaca na ekranie
 */
public class Display {

    public final int width;
    public final int gameWidth;

    public final int informationWidth;
    public final int height;
    public int[] pixels;
    private int yOffset = 0;
    private int xOffset = 0;

    public Display(int width, int height, int gameWidth, int informationWidth) {
        this.width = width;
        this.height = height;
        this.gameWidth = gameWidth;
        this.informationWidth = informationWidth;
        pixels = new int[width * height];
    }

    public void clear() {
        for (int i = 0; i < pixels.length; i++) {
            pixels[i] = 5;
        }
    }

    public void renderTile(int xp, int yp, Tile tile) {
        xp -= xOffset; //jezeli ma sie kamera poruszac
        yp -= yOffset; //jezeli ma sie kamera poruszac
        for (int y = 0; y < tile.sprite.SIZE_BLOCK; y++) {
            int ya = y + yp;
            for (int x = 0; x < tile.sprite.SIZE_BLOCK; x++) {
                int xa = x + xp;
                if (xa < -tile.sprite.SIZE_BLOCK || xa >= gameWidth || ya < 0 || ya >= height) {
                    break;
                }
                if (xa < 0) { //neguje out of bound exception z lewej strony
                    xa = 0;
                }
                pixels[xa + ya * width] = tile.sprite.pixels[x + y * tile.sprite.SIZE_BLOCK];
            }
        }
    }

    public void setOffset(int x0, int y0) {
        this.xOffset = x0;
        this.yOffset = y0;
    }

    public void renderObject(int xp, int yp, Sprite sprite) {
        xp -= xOffset; //jezeli ma sie kamera poruszac
        yp -= yOffset; //jezeli ma sie kamera poruszac
        int col;
        for (int y = 0; y < sprite.SIZE_BLOCK; y++) {
            int ya = y + yp;
            for (int x = 0; x < sprite.SIZE_BLOCK; x++) {
                int xa = x + xp;
                if (xa < -sprite.SIZE_BLOCK || xa >= gameWidth || ya < 0 || ya >= height) {
                    break;
                }
                if (xa < 0) { //neguje out of bound exception z lewej strony
                    xa = 0;
                }
                col = sprite.pixels[x + y * sprite.SIZE_BLOCK];
                if (col == 0xFFA3007F) {
                    continue;
                }
                pixels[xa + ya * width] = col;
            }
        }
    }

    public void renderUI(Graphics g, int winner) {
        int yoffset = 20;
        int xoffset = 40;
        String status;
        Color col = Color.white;
        g.setColor(Color.BLACK);
        g.fillRect((int) gameWidth, 0, width, height);
        g.setColor(Color.WHITE);
        g.setFont(new Font("Verdana", 0, 15));
        if (winner == 4020) {
            
            synchronized (Level.players) {
                Iterator i = Level.players.iterator(); // Must be in synchronized block
                while (i.hasNext()) {
                    Player pom = (Player) i.next();
                    switch (pom.getId()) {
                        case 0:
                            col = Color.red;
                            break;
                        case 1:
                            col = Color.blue;
                            break;
                        case 2:
                            col = Color.green;
                            break;
                        case 3:
                            col = Color.orange;
                            break;
                        default:
                            col = Color.white;

                    }
                    g.setColor(col);
                    g.fillRect(gameWidth + 10, yoffset + 10, 10, 10);
                    g.setColor(Color.white);
                    g.drawString("Player " + (pom.getName()), (int) gameWidth + xoffset, yoffset);
                    yoffset += 20;
                    g.drawString("X: " + (int) pom.x + " Y: " + (int) pom.y, (int) gameWidth + xoffset, yoffset);
                    yoffset += 20;
                    g.drawString("POWER: " + pom.getBombRange(), (int) gameWidth + xoffset, yoffset);
                    yoffset += 20;
                    if (pom.getLives() > 0) {
                        g.drawString("LIVES: " + pom.getLives(), (int) gameWidth + xoffset, yoffset);
                    } else {
                        g.drawString("YOU ARE DEAD", (int) gameWidth + xoffset, yoffset);
                    }
                    yoffset += 20;
                    g.drawString("BOMBS: " + pom.getMaxBombs(), (int) gameWidth + xoffset, yoffset);
                    yoffset += 20;
                    g.drawString("SPEED: " + pom.getSpeed(), (int) gameWidth + xoffset, yoffset);
                    yoffset += 20;
                    status = pom.getDisconnected() ? "DISCONNECTED" : "ACTIVE";
                    g.drawString("STATUS: " + status, (int) gameWidth + xoffset, yoffset);
                    yoffset += 30;
                }
            }
        }else if (winner!=999){
            synchronized (Level.players) {
                Iterator i = Level.players.iterator(); // Must be in synchronized block
                while (i.hasNext()) {
                    Player pom = (Player) i.next();
                    if (pom.getId()==winner){
                        g.drawString("ZWYCIEZCA GRACZ "+(winner+1), (int) gameWidth + xoffset, yoffset);
                        yoffset+=20;
                        g.drawString(pom.getName(), (int) gameWidth + xoffset, yoffset);
                        break;
                    }
                }
            }
            
        }else{
            g.drawString("REMIS", (int) gameWidth + xoffset, yoffset);
        }

    }

}
