/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.graphics;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * klasa czytajaca grafike
 */
public class SpriteSheet implements Serializable{
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    private final String path;
    public final int SIZE;
    public int[] pixels;
    public final int SIZE_BLOCK;
    public static SpriteSheet tiles = new SpriteSheet(640, "/textures/sheets/spritesheet32.png", 32);

    public SpriteSheet(int size, String path, int blockSize) {
        this.path = path;
        this.SIZE = size;
        this.SIZE_BLOCK = blockSize;
        pixels = new int[SIZE * SIZE];
        load();
    }

    private void load() {
        try {
            BufferedImage image = ImageIO.read(SpriteSheet.class.getResourceAsStream(path));
            int w = image.getWidth();
            int h = image.getHeight();
            image.getRGB(0, 0, w, h, pixels, 0, w);
        } catch (IOException ex) {
            Logger.getLogger(SpriteSheet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


}
