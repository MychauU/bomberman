/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.graphics;

import java.io.Serializable;

/**
 *
 * klasa przechowujaca grafike
 */
public class Sprite implements Serializable{
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    public final int SIZE_BLOCK;
    private int x, y;
    public int[] pixels;
    private SpriteSheet sheet;
    
    public final static Sprite power_up = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 8, 4, SpriteSheet.tiles);
    public final static Sprite health_up = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 9, 4, SpriteSheet.tiles);
    public final static Sprite ammo_up = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 10, 4, SpriteSheet.tiles);
    public final static Sprite speed_up = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 11, 4, SpriteSheet.tiles);
    
    public final static Sprite bomb_sprite_0 = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 8, 1, SpriteSheet.tiles);
    public final static Sprite bomb_sprite_1 = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 9, 1, SpriteSheet.tiles);
    public final static Sprite bomb_sprite_2 = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 10, 1, SpriteSheet.tiles);
    public final static Sprite bomb_sprite_3 = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 11, 1, SpriteSheet.tiles);
    
    public final static Sprite flame_sprite_0 = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 8, 2, SpriteSheet.tiles);
    public final static Sprite flame_sprite_1 = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 9, 2, SpriteSheet.tiles);
    public final static Sprite flame_sprite_2 = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 10, 2, SpriteSheet.tiles);
    public final static Sprite flame_sprite_3 = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 11, 2, SpriteSheet.tiles);
    public final static Sprite flame_sprite_4 = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 12, 2, SpriteSheet.tiles);
    public final static Sprite flame_sprite_5 = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 13, 2, SpriteSheet.tiles);

    public final static Sprite border_sprite = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 0, 0, SpriteSheet.tiles);
    public final static Sprite solid_sprite = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 3, 0, SpriteSheet.tiles);
    public final static Sprite box_sprite = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 1, 0, SpriteSheet.tiles);
    public final static Sprite floor_sprite = new Sprite(SpriteSheet.tiles.SIZE_BLOCK, 2, 0, SpriteSheet.tiles);
    public final static Sprite void_sprite = new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0xffff0000);

    public final static Sprite any_player_sprite_dying=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,8,3,SpriteSheet.tiles);
    
    public final static Sprite red_player_sprite_south_0=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0,1,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_south_1=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,1,1,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_south_2=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,2,1,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_south_3=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,3,1,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_south_4=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,4,1,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_south_5=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,5,1,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_south_6=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,6,1,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_south_7=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,7,1,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_north_0=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0,2,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_north_1=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,1,2,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_north_2=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,2,2,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_north_3=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,3,2,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_north_4=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,4,2,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_north_5=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,5,2,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_north_6=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,6,2,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_north_7=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,7,2,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_west_0=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0,3,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_west_1=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,1,3,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_west_2=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,2,3,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_west_3=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,3,3,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_west_4=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,4,3,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_west_5=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,5,3,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_west_6=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,6,3,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_west_7=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,7,3,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_east_0=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0,4,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_east_1=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,1,4,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_east_2=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,2,4,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_east_3=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,3,4,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_east_4=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,4,4,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_east_5=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,5,4,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_east_6=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,6,4,SpriteSheet.tiles);
    public final static Sprite red_player_sprite_east_7=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,7,4,SpriteSheet.tiles);
    
    public final static Sprite blue_player_sprite_south_0=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0,5,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_south_1=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,1,5,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_south_2=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,2,5,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_south_3=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,3,5,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_south_4=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,4,5,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_south_5=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,5,5,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_south_6=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,6,5,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_south_7=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,7,5,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_north_0=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0,6,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_north_1=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,1,6,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_north_2=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,2,6,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_north_3=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,3,6,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_north_4=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,4,6,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_north_5=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,5,6,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_north_6=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,6,6,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_north_7=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,7,6,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_west_0=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0,7,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_west_1=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,1,7,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_west_2=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,2,7,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_west_3=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,3,7,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_west_4=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,4,7,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_west_5=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,5,7,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_west_6=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,6,7,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_west_7=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,7,7,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_east_0=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0,8,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_east_1=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,1,8,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_east_2=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,2,8,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_east_3=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,3,8,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_east_4=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,4,8,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_east_5=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,5,8,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_east_6=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,6,8,SpriteSheet.tiles);
    public final static Sprite blue_player_sprite_east_7=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,7,8,SpriteSheet.tiles);
    
    public final static Sprite green_player_sprite_south_0=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0,9,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_south_1=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,1,9,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_south_2=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,2,9,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_south_3=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,3,9,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_south_4=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,4,9,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_south_5=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,5,9,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_south_6=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,6,9,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_south_7=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,7,9,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_north_0=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0,10,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_north_1=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,1,10,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_north_2=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,2,10,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_north_3=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,3,10,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_north_4=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,4,10,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_north_5=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,5,10,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_north_6=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,6,10,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_north_7=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,7,10,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_west_0=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0,11,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_west_1=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,1,11,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_west_2=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,2,11,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_west_3=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,3,11,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_west_4=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,4,11,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_west_5=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,5,11,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_west_6=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,6,11,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_west_7=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,7,11,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_east_0=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0,12,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_east_1=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,1,12,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_east_2=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,2,12,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_east_3=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,3,12,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_east_4=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,4,12,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_east_5=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,5,12,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_east_6=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,6,12,SpriteSheet.tiles);
    public final static Sprite green_player_sprite_east_7=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,7,12,SpriteSheet.tiles);
    
    public final static Sprite orange_player_sprite_south_0=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0,13,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_south_1=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,1,13,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_south_2=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,2,13,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_south_3=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,3,13,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_south_4=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,4,13,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_south_5=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,5,13,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_south_6=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,6,13,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_south_7=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,7,13,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_north_0=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0,14,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_north_1=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,1,14,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_north_2=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,2,14,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_north_3=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,3,14,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_north_4=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,4,14,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_north_5=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,5,14,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_north_6=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,6,14,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_north_7=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,7,14,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_west_0=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0,15,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_west_1=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,1,15,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_west_2=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,2,15,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_west_3=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,3,15,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_west_4=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,4,15,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_west_5=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,5,15,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_west_6=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,6,15,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_west_7=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,7,15,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_east_0=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,0,16,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_east_1=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,1,16,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_east_2=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,2,16,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_east_3=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,3,16,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_east_4=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,4,16,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_east_5=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,5,16,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_east_6=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,6,16,SpriteSheet.tiles);
    public final static Sprite orange_player_sprite_east_7=new Sprite(SpriteSheet.tiles.SIZE_BLOCK,7,16,SpriteSheet.tiles);
    
    public Sprite(int size, int colour) {
        this.SIZE_BLOCK = size;
        pixels = new int[size * size];
        setColour(colour);
    }

    public Sprite(int size, int x, int y, SpriteSheet sheet) {
        this.SIZE_BLOCK = size;
        this.x = x * size;
        this.y = y * size;
        this.sheet = sheet;
        pixels = new int[size * size];
        load();
    }

    private void load() {
        for (int y = 0; y < SIZE_BLOCK; y++) {
            for (int x = 0; x < SIZE_BLOCK; x++) {
              //  if (sheet.pixels[(x + this.x) + (y + this.y) * sheet.SIZE]==0xffA3007F){
              //      pixels[x + y * SIZE_BLOCK]=0x
              //  }
              //  else{
                    pixels[x + y * SIZE_BLOCK] = sheet.pixels[(x + this.x) + (y + this.y) * sheet.SIZE];
              //  }
            }
        }
    }

    private void setColour(int colour) {
        for (int i = 0; i < SIZE_BLOCK*SIZE_BLOCK; i++) {
            pixels[i]=colour;
        }
    }

}
