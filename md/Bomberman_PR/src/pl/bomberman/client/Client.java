/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.client;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.bomberman.Game;
import pl.bomberman.gui.JMainWindow;

/**
 *
 * klasa obslugujaca polaczaenie z serwerem oraz pozniejsze odbieranie paczek i ich wysylanie
 */
public class Client {

    public static LinkedBlockingQueue<Object> messages;
    public static int PORT=9877;
    protected static List<Object> messagesToSent;
    protected ConnectionToServer connectionToServer;
    protected MessageHandlingClientSide messageHandling;
    protected JMainWindow parentWindow;
    protected Game game;
    private Socket requestSocket;
    private Thread messageHandlingThread;
    
    static{
        messages = new LinkedBlockingQueue<>();
        messagesToSent = new ArrayList<>();
    }
    
    public MessageHandlingClientSide getMessageHandling() {
        return messageHandling;
    }
    

    

    public Client(JMainWindow parent, Game game) {
        this.parentWindow = parent;        
        this.game = game;
    }

    
    //funkcja wywolywana podczas nacisniecia przycisku connect(dolącz)
    public void connect(String name, String ip) {
        try {
            //1. creating a socket to connect to the server
            requestSocket = new Socket(ip, PORT);

            System.out.println("Connected to ip in port 9877");
            //2. get Input and Output streams
            connectionToServer = new ConnectionToServer(requestSocket, this);
            this.send(ip);
            connectionToServer.name = name;
            connectionToServer.ip = ip;
            System.out.println("Connected to " + ip + " in port 9877");
            messageHandling = new MessageHandlingClientSide( this);
            messageHandlingThread = new Thread(messageHandling, "messageHandlingToClientFromQueue");
            messageHandlingThread.start();
            this.parentWindow.getLobby().setConnectButton(false);

        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //wysylanie bez flush
    protected void send(Object obj) {
        connectionToServer.write(obj);
    }

    
    //wysylanie z flush
    protected void sendWithoutFlush(Object obj) {
        connectionToServer.writeWithoutFlush(obj);
    }
    
    
    //zatrzymanie polaczenia podczas wyjscia z gry
    public synchronized void stopConnection() {
        try {
            if (this.connectionToServer != null) {
                this.connectionToServer.out.close();
                this.connectionToServer.in.close();
                this.connectionToServer.socket.close();
                if (this.messageHandling!=null){
                    this.messageHandling.setRunning(false);
                }
                if (this.messageHandlingThread!=null){
                    messageHandlingThread.join(1000);
                }
                Client.messages.clear();
                Client.messagesToSent.clear();
            }
        } catch (InterruptedException | IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
