/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * klasa trzymajaca polaczenie z serwerem oraz streamy do wysylania danych, 
 * odbierania oraz thread na odbieranie danych raw
 * @author MychauU
 */
public class ConnectionToServer {

    ObjectInputStream in;
    ObjectOutputStream out;
    Socket socket;
    public String name;
    public String ip;
    private RecievierOfMessages recieveMsg;
    Client client;

    ConnectionToServer(Socket x, Client client) throws IOException {
        this.client = client;
        this.socket = x;
        this.socket.setTcpNoDelay(true);
        this.out = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
        this.out.flush();
        this.in = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
        this.recieveMsg = new RecievierOfMessages(this);
        
        Thread read = new Thread(recieveMsg, "thread read from server to queue");
        //    read.setDaemon(true);
        read.start();
    }

    ///wyslij dane  do serwera z flush(przepychanie danych)
    protected void write(Object obj) {
        try {
            out.writeObject(obj);
            out.flush();
        } catch (IOException e) {
        }
    }

    //wyslij dane do serwera
    protected void writeWithoutFlush(Object obj) {
        try {
            out.writeObject(obj);
        } catch (IOException e) {
        }
    }

    
    //prywatna klasa przyjmujaca wszystkie dane od serwera
    private class RecievierOfMessages implements Runnable {

        ConnectionToServer connectionToServer;

        public RecievierOfMessages(ConnectionToServer parent) {
            this.connectionToServer = parent;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    Object obj = in.readObject();
                    if (obj != null) {
                        Client.messages.put(obj);
                    }

                } catch (IOException | ClassNotFoundException | InterruptedException ex) {
                    Logger.getLogger(ConnectionToServer.class.getName()).log(Level.SEVERE, null, ex);
                    //zakonczenie polaczenia gdy nieznana klasa lub odczht zostal przerwany, lub socket nie dziala
                    this.connectionToServer.client.stopConnection();
                    break;

                }

            }

        }
    }
}
