/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.client;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import pl.bomberman.entity.mob.DIRECTION;
import static pl.bomberman.client.Client.messages;
import pl.bomberman.entity.Bomb;
import pl.bomberman.entity.Box;
import pl.bomberman.entity.Flame;
import pl.bomberman.entity.PowerUp;
import pl.bomberman.entity.UPGRADE;
import pl.bomberman.entity.mob.Player;
import pl.bomberman.level.Level;
import static pl.bomberman.level.Level.boxes;
import pl.bomberman.sendpack.MESSAGETYPE;
import pl.bomberman.sendpack.MessagePackage;

/**
 *
 * klasa obslugujaca uzyskane obiekty od serwera i od siebie (kolejkuje sie w kolejce)
 */
public class MessageHandlingClientSide implements Runnable {

    Client client;
    private boolean running;

    public MessageHandlingClientSide(Client client) {
        this.client = client;
        running = true;
    }

    public synchronized void setRunning(boolean pom) {
        running = pom;
    }
    
    
    
    //funckja dziala podczas przyjmowania polaczenia i zanim uruchomi sie gra
    @Override
    public void run() {
        while (running) {
            try {
                Object message = messages.take();
                if (message instanceof MessagePackage) {
                    MessagePackage obj = (MessagePackage) message;
                    if (null != obj.message) {
                        switch (obj.message) {
                            case SENDPLAYERSNAMEFROMSERVER: {
                                String tab[] = (String[]) obj.obj;
                                int j = 0;
                                synchronized (Level.players) {
                                    Iterator i = Level.players.iterator(); // Must be in synchronized block
                                    while (i.hasNext() && j < tab.length) {
                                        Player pom = (Player) i.next();
                                        pom.setName(tab[j++]);
                                    }
                                }
                            }
                            break;
                            case STARTGAME:
                                client.parentWindow.setGameComp();
                                this.setRunning(false);
                                break;
                            case IDENTIFY:{
                                this.client.game.setId(obj.id);
                                client.parentWindow.getLobby().setStartButton(true);
                                MessagePackage pom = new MessagePackage(client.connectionToServer.name, MESSAGETYPE.SENDPLAYERNAMEFROMCLIENT, client.game.getId());
                                pom.additionalInformation = client.connectionToServer.socket.getInetAddress();
                                client.send(pom);
                            }
                                break;
                            case SENDLEVEL:
                                this.client.game.setLevel((Level) obj.obj);
                                break;
                            case SENDPLAYERSFROMSERVER: {
                                List<Player> msg = (List<Player>) obj.obj;
                                msg.stream().forEach((pr) -> {
                                    Level.players.add(pr);
                                });
                            }
                            break;
                            case SENDBOXESFROMSERVER: {
                                List<Box> msg = (List<Box>) obj.obj;
                                msg.stream().forEach((pr) -> {
                                    Level.boxes.add(pr);
                                });
                            }
                            break;
                            case CREATENEWGAME: {
                                client.send(obj);
                            }
                            break;
                            default:
                                break;
                        }
                    }

                }

                //System.out.println("Message Received: " + message);
            } catch (InterruptedException e) {

            }
        }
    }

    //funkcja wykonujaca sie 60 ups wywolywana przez obiekt Game handluje wszystkie obiekty w kolejce messages
    //dodaje do listy message ktore trzeba wyslac (wysyla paczkami aby zredukowac udorzenia tcp (czy moze wysylac))
    public void handleNewData() throws IOException {
        while (true) {
            Object message = messages.poll();
            if (message == null) {
                break;
            } else if (message instanceof MessagePackage) {
                MessagePackage obj = (MessagePackage) message;
                if (null != obj.message) {
                    switch (obj.message) {
                        case MOVEMENTFROMSERVER: {
                            Point2D.Double newXY = (Point2D.Double) obj.obj;
                            Level.players.get(obj.id).setCooridinates(newXY);
                            Level.players.get(obj.id).setWalking();
                            Level.players.get(obj.id).setDirection((DIRECTION) obj.additionalInformation);
                        }
                        case MOVEMENTFROMCLIENT: {
                            Client.messagesToSent.add(obj);
                            // client.send(message);
                        }
                        break;

                        case CREATEBOMBFROMCLIENT: {
                            Client.messagesToSent.add(obj);
                            //   client.send(message);
                        }
                        break;
                        case CREATEBOMBFROMSERVER: {
                            Point2D.Double xy = (Point2D.Double) obj.obj;
                            int bombRange = (int) obj.additionalInformation;
                            int id = obj.id;
                            Level.bombs.add(new Bomb(xy.x, xy.y, this.client.game.getLevel(), bombRange));
                            Level.players.get(id).decBombsInHand();
                            //System.out.println("Bomb Received: " + message);
                        }
                        break;
                        case DELETEBOMBFROMSERVER: {
                            Point2D.Double xy = (Point2D.Double) obj.obj;
                            int id = obj.id;
                            synchronized (Level.bombs) {
                                Iterator i = Level.bombs.iterator(); // Must be in synchronized block
                                while (i.hasNext()) {
                                    Bomb pom = (Bomb) i.next();
                                    if (pom.x == xy.x && pom.y == xy.y) {
                                        pom.detonate();
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                        case CREATEFIREFROMSERVER: {
                            Point2D.Double xy = (Point2D.Double) obj.obj;
                            int id = obj.id;
                            int tab[] = (int[]) obj.additionalInformation;
                            Flame flame = new Flame(xy.x, xy.y, obj.id, this.client.game.getLevel());
                            flame.setLeftBound(  tab[0]);
                            flame.setRightBound (tab[2]);
                            flame.setUpBound (tab[1]);
                            flame.setDownBound(tab[3]);
                            Level.flames.add(flame);
                        }
                        break;
                        case REMOVEBOXFROMSERVER: {
                            Point2D.Double xy = (Point2D.Double) obj.obj;
                            //   podobno wolne nawet 15x razy
                            // Level.boxes.stream().filter((pom) -> (pom.x== xy.x && pom.y ==xy.y)).forEach((Box pom)-> pom.detonate()); 
                            synchronized (Level.boxes) {
                                Iterator i = boxes.iterator(); // Must be in synchronized block
                                while (i.hasNext()) {
                                    Box pom = (Box) i.next();
                                    if (pom.x == xy.x && pom.y == xy.y) {
                                        pom.detonate();
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                        case CREATEPOWERUPFROMSERVER: {
                            Point2D.Double xy = (Point2D.Double) obj.obj;
                            UPGRADE pom = (UPGRADE) obj.additionalInformation;
                            Level.powerups.add(new PowerUp(xy.x, xy.y, this.client.game.getLevel(), pom));
                        }
                        break;
                        case REMOVEPOWERUPFROMSERVER: {
                            Point2D.Double xy = (Point2D.Double) obj.obj;
                            //   podobno wolne nawet 15x razy
                            // Level.boxes.stream().filter((pom) -> (pom.x== xy.x && pom.y ==xy.y)).forEach((Box pom)-> pom.detonate());

                            synchronized (Level.powerups) {
                                Iterator i = Level.powerups.iterator(); // Must be in synchronized block
                                while (i.hasNext()) {
                                    PowerUp pom = (PowerUp) i.next();
                                    if (pom.x == xy.x && pom.y == xy.y) {
                                        synchronized (Level.players) {
                                            Iterator g = Level.players.iterator(); // Must be in synchronized block
                                            while (g.hasNext()) {
                                                Player pl = (Player) g.next();
                                                if (pl.getId() == obj.id) {
                                                    pom.powerUpPlayer(pl);
                                                    break;
                                                }
                                            }
                                        }
                                        pom.remove();
                                        break;
                                    }

                                }
                            }
                        }
                        break;
                        case DECLIFE: {
                            synchronized (Level.players) {
                                Iterator g = Level.players.iterator(); // Must be in synchronized block
                                while (g.hasNext()) {
                                    Player pom = (Player) g.next();
                                    if (pom.getId() == obj.id) {
                                        pom.decLives();
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                        case DISCONNECTEDPLAYERFROMSERVER: {
                            synchronized (Level.players) {
                                Iterator g = Level.players.iterator(); // Must be in synchronized block
                                while (g.hasNext()) {
                                    Player pom = (Player) g.next();
                                    if (pom.getId() == obj.id) {
                                        pom.setDisconnected(true);
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                        case WINNERPLAYERFROMSERVER: {
                            this.client.game.setWinner(obj.id);
                        }
                        
                        default:
                            break;
                    }
                }

            }

        }
        //skolejkowanie messages do wyslania
        for (Iterator<Object> it = Client.messagesToSent.iterator(); it.hasNext();) {
            Object pom = it.next();
            client.sendWithoutFlush(pom);

        }
        Client.messagesToSent.clear();
        client.connectionToServer.out.flush();
    }
    
    

}
