/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman;

import pl.bomberman.gui.JMainWindow;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.IOException;
import pl.bomberman.level.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import pl.bomberman.entity.mob.Player;
import pl.bomberman.graphics.Display;
import pl.bomberman.input.Keyboard;

/**
 *
 * klasa ktora przechowuje rozmiar okna, taktyke renderowania zdjec, 
 * ograniczniki czasowe  dla update (ile razy update sie wykona na sekunde
 * 
 */
public class Game extends Canvas implements Runnable {

    public static final String title = "BomberMan";
    public static final double scale = 1;
    public static final int width = 1080;
    public static final int height = (width / 16) * 9;
    public static final int informationWidth = (int)((int)32 * 7 );
    public static final int  gameWidth = width - 56 - informationWidth;
    //skalowanie gry (czyli mozna zmniejszyc rozmiar okna i renderowanych pikseli)

    //
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    //buffor klatek
    private final BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    //piksele renderowania calego ekranu
    private final int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
    private boolean running = false;
    private Thread thread;
    private JFrame frame;
    private final JMainWindow parent;
    private Display display;
    private Keyboard keyboard;
    private Level level;
    private int id;
    private int winner=4020;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Game(JMainWindow parent) {
        this.parent = parent;
        id=999;
    }
    
    public Level getLevel() {
        return level;
        
    }

    //funkcja tworzaca nowe okno i nowy watek  w ktorej bedzie widac przebieg gry
    public synchronized void start() {
        frame = new JFrame();
        //trzeba ustawic aby po nacisnieciu krzyzyka program gry zakonczyl sie a nie tylko wylaczyc zawartosc wyswietlana
        WindowListener exitListener = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                stop();
                parent.setCompAfterLeavingGame();
                parent.setVisible(true);
                parent.getLobby().setStartButton(false);
                parent.getLobby().setConnectButton(true);
                parent.stopConnection();
            }
        };
        frame.addWindowListener(exitListener);
        display = new Display(width, height, (int)gameWidth, (int)informationWidth);
        keyboard = new Keyboard();
        frame.addKeyListener(keyboard);
        this.setPlayers();
        frame.setResizable(false);
        frame.setTitle(title);
        this.winner=4020;
        
        int dimWidth=(int) ((int)width * scale);
        int dimHeight=(int) ((int)height * scale);
        Dimension size = new Dimension(dimWidth, dimHeight);
        this.setPreferredSize(size);  //canvas metoda ustawia rozmiar ekranu
        //dodaje do komponentu  jframe komponent canvas
        frame.add(this);
        //jezeli elementy sa mniejsze od dimension sa one rozszerzane aby wypelnic okno
        frame.pack();
        //    frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setVisible(true);
        //frame.requestFocusInWindow();
        frame.requestFocus();
        running = true;
        thread = new Thread(this, "game_app");
        thread.start();
    }

    //metoda konczaca wykonanie operacji run
    public synchronized void stop() {
        running = false;
        try {
            thread.join();
            parent.setVisible(true);
        } catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    //glowna funkcja dzialajaca podczas grania
    @Override
    public void run() {
        long lastTime = System.nanoTime(); //czas w ns
        long timer = System.currentTimeMillis(); //czas w ms
        final double ns = 1000000000.0 / 60.0; //czas na wykonanie jednego update nanosekundach przy 60fps
        double delta = 0;
        int frames = 0;  // przechowuje ilosc przetworzonych klatek w render (to nie znaczy ze wszystkie sa zapisane (triple buffer)
        int updates = 0; //przechowuje ile razy updateowalismy gre (powinno byc  ~60fps zgodnie z delta
        //  this.requestFocus();
        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns; //jezeli roznica czasu pomiedz renderowanie a update jest wieksza niz jeden frame
            lastTime = now;
            while (delta >= 1) {
                this.update();
                updates++;
                delta--;
            }
            render();
            frames++;
            //tutaj informacja  o ups  i fps
            if (System.currentTimeMillis() - timer > 1000) { // raz na sekunde
                timer += 1000;
                frame.setTitle(title + " | " + updates + " ups, " + frames + " fps" + " | ");
                updates = 0;
                frames = 0;
            }
        }
    }

    //max 60ups
    public void update() {
        frame.requestFocus();
        keyboard.update();
        level.update();
        try {
            JMainWindow.client.getMessageHandling().handleNewData();
        } catch (IOException ex) {
            Logger.getLogger(Game.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        

    }

    //unlimited  as fast as possible
    public void render() {
        //strategia ufforowania 3 klatek zanim sie pojawia najstarsza
        BufferStrategy bs = getBufferStrategy();
        if (bs == null) {
            createBufferStrategy(3);
            return;
        }
        //czyszczenie pikseli ekranu gry(niektore wartosci moga sie nie zmieniac( np ui)
        display.clear();
        
        //renderowanie bez ruchu kamery
        level.render(display);

        
        //renderowanie z ruchem kamery
     //   level.render((int)player.x-gameWidth/2,(int) player.y-height/2, display);
     
        
        System.arraycopy(display.pixels, 0, pixels, 0, pixels.length);


        Graphics g = bs.getDrawGraphics();

        //interfejs
        //gra
        g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
        display.renderUI(g,this.winner);
        
        g.dispose();
        bs.show();
    }

    public void setLevel(Level level) {
        this.level = level;
        Level.boxes.clear();
        Level.flames.clear();
        Level.bombs.clear();
        Level.players.clear();
        Level.powerups.clear();
        
        
    }
    
    
    //ustawienie klawiatury dla odpowiedniego Playera
    private void setPlayers(){
        Level.players.stream().filter((pom) -> (pom.getId()==this.id)).forEach((Player pom) -> {
            pom.setKeyboard(this.keyboard);
        });
        
    }
    
    //ustalenie zwyciezcy
    public void setWinner(int id) {
        this.winner=id;
    }


}
