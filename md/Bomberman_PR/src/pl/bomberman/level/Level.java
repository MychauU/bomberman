/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.level;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import pl.bomberman.entity.Bomb;
import pl.bomberman.entity.Box;
import pl.bomberman.entity.Flame;
import pl.bomberman.entity.PowerUp;
import pl.bomberman.entity.mob.Player;
import pl.bomberman.graphics.Display;
import pl.bomberman.graphics.Sprite;
import pl.bomberman.level.tile.Tile;

/**
 *
 * @author MychauU
 */
public class Level implements Serializable {

    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    private final String path;
    private int[] levelPixels;
    private int width, height;
    private final Random random;
    private int blockSizeExp;
    public static List<Box> boxes;
    public static List<Flame> flames;
    public static List<Bomb> bombs;
    public static List<Player> players;
    public static List<PowerUp> powerups;

    static{
        boxes = Collections.synchronizedList(new ArrayList<>());
        flames = Collections.synchronizedList(new ArrayList<>());
        bombs = Collections.synchronizedList(new ArrayList<>());
        players = Collections.synchronizedList(new ArrayList<>());
        powerups = Collections.synchronizedList(new ArrayList<>());
    }
    
    public int getBlockSizeExp() {
        return blockSizeExp;
    }

    private void setBlockSizeExp(int blockSizeExp) {
        int i = 0;
        do {
            blockSizeExp = blockSizeExp >> 1;
            i++;
        } while (blockSizeExp > 0);
        this.blockSizeExp = --i;
    }

    public Level(String path) {
        this.path = path;
        random = new Random();
        setBlockSizeExp(Sprite.void_sprite.SIZE_BLOCK);
        loadLevel();
        generateLevel();
    }
    

    //funkcja generujaca skrzynki
    private void generateLevel() {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                switch (levelPixels[x + y * width]) {
                    case 0xFF00FF00: //zielony
                        if (random.nextInt(4) < 2) {
                            //stworz paczke entity box
                            boxes.add(new Box(((x << blockSizeExp) + Sprite.void_sprite.SIZE_BLOCK / 2), ((y << blockSizeExp) + Sprite.void_sprite.SIZE_BLOCK / 2), Sprite.box_sprite, this));
                            levelPixels[x + y * width] = 0xFFFFFFFF;  //bialy
                        } else {
                            levelPixels[x + y * width] = 0xFFFFFFFF; //bialy
                        }
                        break;
                    case 0xFFFF0000:
                        levelPixels[x + y * width] = 0xFFFFFFFF;  //bialy floor tile

                    default:
                }
            }
        }
    }

    private void loadLevel() {
        try {
            BufferedImage image = ImageIO.read(Level.class.getResource(path));
            width = image.getWidth();
            height = image.getHeight();
            levelPixels = new int[width * height];
            image.getRGB(0, 0, width, height, levelPixels, 0, width);
        } catch (IOException ex) {
            Logger.getLogger(Level.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    public void update() {
        synchronized (bombs) {
            Iterator i = bombs.iterator(); // Must be in synchronized block
            while (i.hasNext()){
                Bomb pom=(Bomb)i.next();
                pom.update();
                if (pom.isRemoved()) {
                    i.remove();
                }
            }
        }
        synchronized (boxes) {
            Iterator i = boxes.iterator(); // Must be in synchronized block
            while (i.hasNext()){
                Box pom=(Box)i.next();
                pom.update();
                if (pom.isRemoved()) {
                    i.remove();
                }
            }
        }
        synchronized (flames) {
            Iterator i = flames.iterator(); // Must be in synchronized block
            while (i.hasNext()){
                Flame pom=(Flame)i.next();
                pom.update();
                if (pom.isRemoved()) {
                    i.remove();
                }
            }
        }
        synchronized (powerups) {
            Iterator i = powerups.iterator(); // Must be in synchronized block
            while (i.hasNext()){
                PowerUp pom=(PowerUp)i.next();
                pom.update();
                if (pom.isRemoved()) {
                    i.remove();
                }
            }
        }
        synchronized (players) {
            Iterator i = players.iterator(); // Must be in synchronized block
            while (i.hasNext()){
                Player pom=(Player)i.next();
                if (!pom.isRemoved()) {
                    pom.update();
                }
            }
           
        }

    }

    public void render(int xCamera, int yCamera, Display display) {
        display.setOffset(xCamera, yCamera);
        int x0 = xCamera >> 5;
        int x1 = (xCamera + display.gameWidth + Sprite.void_sprite.SIZE_BLOCK) >> 5;
        int y1 = (yCamera + display.height + Sprite.void_sprite.SIZE_BLOCK) >> 5;
        int y0 = yCamera >> 5;
        for (int y = y0; y < y1; y++) {
            for (int x = x0; x < x1; x++) {
                //tutaj z pikseli szuka tile i renderuje go
                getTile(x, y).render(x << blockSizeExp, y << blockSizeExp, display);

            }
        }
        synchronized (boxes) {
            Iterator i = boxes.iterator(); // Must be in synchronized block
            while (i.hasNext()){
                Box pom=(Box)i.next();
                pom.render(display);
            }
        }
        synchronized (powerups) {
            Iterator i = powerups.iterator(); // Must be in synchronized block
            while (i.hasNext()){
                PowerUp pom=(PowerUp)i.next();
                pom.render(display);
            }
        }
        synchronized (bombs) {
            Iterator i = bombs.iterator(); // Must be in synchronized block
            while (i.hasNext()){
                Bomb pom=(Bomb)i.next();
                pom.render(display);
            }
        }
        synchronized (players) {
            Iterator i = players.iterator(); // Must be in synchronized block
            while (i.hasNext()){
                Player pom=(Player)i.next();
                pom.render(display);
            }
        }
        synchronized (flames) {
            Iterator i = flames.iterator(); // Must be in synchronized block
            while (i.hasNext()){
                Flame pom=(Flame)i.next();
                pom.render(display);
            }
        }
    }

    public void render(Display display) {
        int x1 = (display.gameWidth + Sprite.void_sprite.SIZE_BLOCK) >> 5;
        int y1 = (display.height + Sprite.void_sprite.SIZE_BLOCK) >> 5;
        for (int y = 0; y < y1; y++) {
            for (int x = 0; x < x1; x++) {
                //konwersja z pikseli
                getTile(x, y).render(x << blockSizeExp, y << blockSizeExp, display);
            }
        }
        synchronized (boxes) {
            Iterator i = boxes.iterator(); // Must be in synchronized block
            while (i.hasNext()){
                Box pom=(Box)i.next();
                pom.render(display);
            }
        }
        synchronized (powerups) {
            Iterator i = powerups.iterator(); // Must be in synchronized block
            while (i.hasNext()){
                PowerUp pom=(PowerUp)i.next();
                pom.render(display);
            }
        }
        synchronized (bombs) {
            Iterator i = bombs.iterator(); // Must be in synchronized block
            while (i.hasNext()){
                Bomb pom=(Bomb)i.next();
                pom.render(display);
            }
        }
        synchronized (players) {
            Iterator i = players.iterator(); // Must be in synchronized block
            while (i.hasNext()){
                Player pom=(Player)i.next();
                if (!pom.isRemoved())
                    pom.render(display);
            }
        }
        synchronized (flames) {
            Iterator i = flames.iterator(); // Must be in synchronized block
            while (i.hasNext()){
                Flame pom=(Flame)i.next();
                pom.render(display);
            }
        }
        
    }

    public Tile getTile(int x, int y) {
        if (x >= width || x < 0 || y < 0 || y >= height) {
            return Tile.void_tile;
        } else if (levelPixels[x + y * width] == Tile.wall_tile_col) {
            return Tile.wall_tile;
        } else if (levelPixels[x + y * width] == Tile.border_tile_col) {
            return Tile.border_tile;
        } else if (levelPixels[x + y * width] == Tile.floor_tile_col) {
            return Tile.floor_tile;
        }
        return Tile.void_tile;
    }
    
    public synchronized static void clearLevelElements(){
        Level.bombs.clear();
        Level.boxes.clear();
        Level.players.clear();
        Level.powerups.clear();
        Level.flames.clear();
    }


}
