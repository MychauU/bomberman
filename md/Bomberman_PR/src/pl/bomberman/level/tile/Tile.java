/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.level.tile;

import java.io.Serializable;
import pl.bomberman.graphics.Display;
import pl.bomberman.graphics.Sprite;

/**
 *
 * @author MychauU
 */
public abstract class Tile implements Serializable{
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    public Sprite sprite;
    public static final Tile wall_tile = new SolidTile(Sprite.solid_sprite);
    public static final Tile border_tile = new SolidTile(Sprite.border_sprite);
    public static final Tile box_tile = new BoxTile(Sprite.box_sprite);
    public static final Tile floor_tile = new FloorTile(Sprite.floor_sprite);
    public static final Tile void_tile = new SolidTile(Sprite.void_sprite);

   public static final int wall_tile_col=0xFF000000;
   public static final int border_tile_col=0xFF0000FF;
   public static final int box_tile_col=0xFF00FF00;
   public static final int floor_tile_col=0xFFFFFFFF;
    
    public Tile(Sprite sprite) {
        this.sprite = sprite;
    }
    //czy jest kolizyjny (czy mozna przez niego przejsc lub inne akcje)
    public abstract boolean isSolid(); 
    //renderowanie obiektu
    public abstract void render(int x, int y, Display display);
    //czy da sie zniszczyc
    public abstract boolean isBreakable();
   
}
