/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.level.tile;

import java.io.Serializable;
import pl.bomberman.graphics.Display;
import pl.bomberman.graphics.Sprite;

/**
 *
 * @author MychauU
 */
public class SolidTile extends Tile implements Serializable{
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    public SolidTile(Sprite sprite) {
        super(sprite);
    }

    @Override
    public boolean isSolid() {
        return true;
    }

    @Override
    public void render(int x, int y, Display display) {
        display.renderTile(x , y , this);
    }

    @Override
    public boolean isBreakable() {
        return false;
    }
    
}
