/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.entity;

import java.io.Serializable;

/**
 *
 * klasa enum opisujaca nagrodu
 */
public enum UPGRADE implements Serializable {

    POWER_UP(0),
    HEALTH_UP(1),
    AMMO_UP(2),
    SPEED_UP(3);
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    private final int upgrade;

    private UPGRADE(int upgrade) {
        this.upgrade = upgrade;
    }

    public int GETUPGRADE() {
        return upgrade;
    }
}
