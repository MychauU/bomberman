/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.entity.mob;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.io.Serializable;
import pl.bomberman.client.Client;
import pl.bomberman.graphics.Display;
import pl.bomberman.graphics.Sprite;
import pl.bomberman.input.Keyboard;
import pl.bomberman.level.Level;
import pl.bomberman.sendpack.MESSAGETYPE;
import pl.bomberman.sendpack.MessagePackage;

/**
 *
 * klasa player -gracz
 */
public class Player extends Mob implements Serializable {

    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    private Keyboard keyboard;

    public void setKeyboard(Keyboard keyboard) {
        this.keyboard = keyboard;
    }
    
    private int bombsInHand;
    private int maxBombs = 1;
    private String name;
    private boolean disconnected;
    private int rechargeTime;
    private int bombRange;
    private boolean dying;


    //konstruktor client playerow
    public Player(double x, double y, int id, Level level) {
        this.dying = false;
        this.bombRange = 3;
        this.rechargeTime = 0;
        this.disconnected = false;
        this.bombsInHand = 1;
        this.lives = 2;
        this.level = level;
        this.id = id;
        this.keyboard = null;
        this.x = x;
        this.y = y;
        sprite = Sprite.red_player_sprite_south_0;
        this.blockSizeExp = level.getBlockSizeExp();
    }

    @Override
    public void decLives() {
        if (!immortal) {
            super.decLives();
            hitted = true;
            immortal = true;
            rechargeTime = 240;
        }

    }
    
    public void setDisconnected(boolean pom){
        this.disconnected=pom;
    }
    public boolean getDisconnected( ){
        return this.disconnected;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public int getMaxBombs() {
        return maxBombs;
    }
    public void setWalking(){
        walking=true;
    }
    
    public void setDirection(DIRECTION pom){
        direction=pom;
    }
    
     public int getSpeed() {
        return (int)speed;
    }
    public int getBombRange() {
        return bombRange;
    }
    

    public void incMaxBombs() {
        this.maxBombs += 1;
    }

    public void incBombsInHand() {
        this.bombsInHand += 1;
    }
    
    public void decBombsInHand() {
        this.bombsInHand += 1;
    }

    public void incLives() {
        this.lives += 1;
    }

    public void incBombRange() {
        this.bombRange += 1;
    }

    public void incSpeed() {
        if (this.speed < 6) {
            this.speed *= 1.5;
            if (this.speed > 6) {
                this.speed = 6;
            }
        }
    }
    
    public synchronized void setCooridinates(Point2D.Double xy) {
        this.x = xy.x;
        this.y = xy.y;
    }

    public synchronized Point2D.Double getCooridinates() {
        return new Point2D.Double(this.x, this.y);
    }

    @Override
    public void update() {
        if (dying) {
            if (rechargeTime < 0) {
                remove();
                return;
            } else {
                rechargeTime--;
                return;
            }
        }
        if (immortal) {
            if (rechargeTime < 0) {
                immortal = false;
                hitted = false;
            } else {
                rechargeTime--;
            }

        }
        if (lives <= 0) {
            keyboard=null;
            dying = true;
            rechargeTime = 120;
            System.out.println("Zgineles");
            return;
        }

        int xa = 0;
        int ya = 0;
        if (anim < 7500) {
            anim++;
        } else {
            anim = 0;
        }
        if (keyboard != null) {
            if (keyboard.up) {
                //stare
                //ya-=speed;
                //
                ya -= 1;
            }
            if (keyboard.down) {
                ya += 1;
            }
            if (keyboard.left) {
                xa -= 1;
            }
            if (keyboard.right) {
                xa += 1;
            }
            if (keyboard.space) {
                //send message plant bomb
                int xt = (((int) x >> this.blockSizeExp) << this.blockSizeExp);
                xt += Sprite.void_sprite.SIZE_BLOCK >> 1;
                int yt = (((int) y >> this.blockSizeExp) << this.blockSizeExp);
                yt += Sprite.void_sprite.SIZE_BLOCK >> 1;
                MessagePackage pom=new MessagePackage(new Point2D.Double(xt, yt), MESSAGETYPE.CREATEBOMBFROMCLIENT, this.id);
                Client.messages.add(pom);
                keyboard.space=false;
            }
            if (xa != 0 || ya != 0) {
                setDirection(xa, ya);
                //send message walking
                MessagePackage pom=new MessagePackage(new Point(xa, ya), MESSAGETYPE.MOVEMENTFROMCLIENT, this.id);
                Client.messages.add(pom);
                walking = true;
            } else {
                walking = false;
            }
        }
        else {
            walking=false;
        }

    }

    
    @Override
    public void render(Display display) {
        if (dying) {
            this.sprite = Sprite.any_player_sprite_dying;
            display.renderObject((int) x - (this.sprite.SIZE_BLOCK >> 1), (int) y - (this.sprite.SIZE_BLOCK >> 1), this.sprite);
            return;
        }
        if (null != direction) {
            if (id == 0) {
                switch (direction) {
                    case NORTH:
                        sprite = Sprite.red_player_sprite_north_0;
                        if (walking) {
                            if (anim % 40 > 35) {
                                sprite = Sprite.red_player_sprite_north_7;
                            } else if (anim % 40 > 30) {
                                sprite = Sprite.red_player_sprite_north_6;
                            } else if (anim % 40 > 25) {
                                sprite = Sprite.red_player_sprite_north_5;
                            } else if (anim % 40 > 20) {
                                sprite = Sprite.red_player_sprite_north_4;
                            } else if (anim % 40 > 15) {
                                sprite = Sprite.red_player_sprite_north_3;
                            } else if (anim % 40 > 10) {
                                sprite = Sprite.red_player_sprite_north_2;
                            } else {
                                sprite = Sprite.red_player_sprite_north_1;
                            }
                        }
                        break;
                    case SOUTH:
                        sprite = Sprite.red_player_sprite_south_0;
                        if (walking) {
                            if (anim % 40 > 35) {
                                sprite = Sprite.red_player_sprite_south_7;
                            } else if (anim % 40 > 30) {
                                sprite = Sprite.red_player_sprite_south_6;
                            } else if (anim % 40 > 25) {
                                sprite = Sprite.red_player_sprite_south_5;
                            } else if (anim % 40 > 20) {
                                sprite = Sprite.red_player_sprite_south_4;
                            } else if (anim % 40 > 15) {
                                sprite = Sprite.red_player_sprite_south_3;
                            } else if (anim % 40 > 10) {
                                sprite = Sprite.red_player_sprite_south_2;
                            } else {
                                sprite = Sprite.red_player_sprite_south_1;
                            }
                        }
                        break;
                    case WEST:
                        sprite = Sprite.red_player_sprite_west_0;
                        if (walking) {
                            if (anim % 40 > 35) {
                                sprite = Sprite.red_player_sprite_west_7;
                            } else if (anim % 40 > 30) {
                                sprite = Sprite.red_player_sprite_west_6;
                            } else if (anim % 40 > 25) {
                                sprite = Sprite.red_player_sprite_west_5;
                            } else if (anim % 40 > 20) {
                                sprite = Sprite.red_player_sprite_west_4;
                            } else if (anim % 40 > 15) {
                                sprite = Sprite.red_player_sprite_west_3;
                            } else if (anim % 40 > 10) {
                                sprite = Sprite.red_player_sprite_west_2;
                            } else {
                                sprite = Sprite.red_player_sprite_west_1;
                            }
                        }
                        break;
                    case EAST:
                        sprite = Sprite.red_player_sprite_east_0;
                        if (walking) {
                            if (anim % 40 > 35) {
                                sprite = Sprite.red_player_sprite_east_7;
                            } else if (anim % 40 > 30) {
                                sprite = Sprite.red_player_sprite_east_6;
                            } else if (anim % 40 > 25) {
                                sprite = Sprite.red_player_sprite_east_5;
                            } else if (anim % 40 > 20) {
                                sprite = Sprite.red_player_sprite_east_4;
                            } else if (anim % 40 > 15) {
                                sprite = Sprite.red_player_sprite_east_3;
                            } else if (anim % 40 > 10) {
                                sprite = Sprite.red_player_sprite_east_2;
                            } else {
                                sprite = Sprite.red_player_sprite_east_1;
                            }
                        }
                        break;
                    default:
                        sprite = Sprite.red_player_sprite_south_0;
                        break;
                }
            } else if (id == 1) {
                switch (direction) {
                    case NORTH:
                        sprite = Sprite.blue_player_sprite_north_0;
                        if (walking) {
                            if (anim % 40 > 35) {
                                sprite = Sprite.blue_player_sprite_north_7;
                            } else if (anim % 40 > 30) {
                                sprite = Sprite.blue_player_sprite_north_6;
                            } else if (anim % 40 > 25) {
                                sprite = Sprite.blue_player_sprite_north_5;
                            } else if (anim % 40 > 20) {
                                sprite = Sprite.blue_player_sprite_north_4;
                            } else if (anim % 40 > 15) {
                                sprite = Sprite.blue_player_sprite_north_3;
                            } else if (anim % 40 > 10) {
                                sprite = Sprite.blue_player_sprite_north_2;
                            } else {
                                sprite = Sprite.blue_player_sprite_north_1;
                            }
                        }
                        break;
                    case SOUTH:
                        sprite = Sprite.blue_player_sprite_south_0;
                        if (walking) {
                            if (anim % 40 > 35) {
                                sprite = Sprite.blue_player_sprite_south_7;
                            } else if (anim % 40 > 30) {
                                sprite = Sprite.blue_player_sprite_south_6;
                            } else if (anim % 40 > 25) {
                                sprite = Sprite.blue_player_sprite_south_5;
                            } else if (anim % 40 > 20) {
                                sprite = Sprite.blue_player_sprite_south_4;
                            } else if (anim % 40 > 15) {
                                sprite = Sprite.blue_player_sprite_south_3;
                            } else if (anim % 40 > 10) {
                                sprite = Sprite.blue_player_sprite_south_2;
                            } else {
                                sprite = Sprite.blue_player_sprite_south_1;
                            }
                        }
                        break;
                    case WEST:
                        sprite = Sprite.blue_player_sprite_west_0;
                        if (walking) {
                            if (anim % 40 > 35) {
                                sprite = Sprite.blue_player_sprite_west_7;
                            } else if (anim % 40 > 30) {
                                sprite = Sprite.blue_player_sprite_west_6;
                            } else if (anim % 40 > 25) {
                                sprite = Sprite.blue_player_sprite_west_5;
                            } else if (anim % 40 > 20) {
                                sprite = Sprite.blue_player_sprite_west_4;
                            } else if (anim % 40 > 15) {
                                sprite = Sprite.blue_player_sprite_west_3;
                            } else if (anim % 40 > 10) {
                                sprite = Sprite.blue_player_sprite_west_2;
                            } else {
                                sprite = Sprite.blue_player_sprite_west_1;
                            }
                        }
                        break;
                    case EAST:
                        sprite = Sprite.blue_player_sprite_east_0;
                        if (walking) {
                            if (anim % 40 > 35) {
                                sprite = Sprite.blue_player_sprite_east_7;
                            } else if (anim % 40 > 30) {
                                sprite = Sprite.blue_player_sprite_east_6;
                            } else if (anim % 40 > 25) {
                                sprite = Sprite.blue_player_sprite_east_5;
                            } else if (anim % 40 > 20) {
                                sprite = Sprite.blue_player_sprite_east_4;
                            } else if (anim % 40 > 15) {
                                sprite = Sprite.blue_player_sprite_east_3;
                            } else if (anim % 40 > 10) {
                                sprite = Sprite.blue_player_sprite_east_2;
                            } else {
                                sprite = Sprite.blue_player_sprite_east_1;
                            }
                        }
                        break;
                    default:
                        sprite = Sprite.blue_player_sprite_south_0;
                        break;
                }
            } else if (id == 2) {
                switch (direction) {
                    case NORTH:
                        sprite = Sprite.green_player_sprite_north_0;
                        if (walking) {
                            if (anim % 40 > 35) {
                                sprite = Sprite.green_player_sprite_north_7;
                            } else if (anim % 40 > 30) {
                                sprite = Sprite.green_player_sprite_north_6;
                            } else if (anim % 40 > 25) {
                                sprite = Sprite.green_player_sprite_north_5;
                            } else if (anim % 40 > 20) {
                                sprite = Sprite.green_player_sprite_north_4;
                            } else if (anim % 40 > 15) {
                                sprite = Sprite.green_player_sprite_north_3;
                            } else if (anim % 40 > 10) {
                                sprite = Sprite.green_player_sprite_north_2;
                            } else {
                                sprite = Sprite.green_player_sprite_north_1;
                            }
                        }
                        break;
                    case SOUTH:
                        sprite = Sprite.green_player_sprite_south_0;
                        if (walking) {
                            if (anim % 40 > 35) {
                                sprite = Sprite.green_player_sprite_south_7;
                            } else if (anim % 40 > 30) {
                                sprite = Sprite.green_player_sprite_south_6;
                            } else if (anim % 40 > 25) {
                                sprite = Sprite.green_player_sprite_south_5;
                            } else if (anim % 40 > 20) {
                                sprite = Sprite.green_player_sprite_south_4;
                            } else if (anim % 40 > 15) {
                                sprite = Sprite.green_player_sprite_south_3;
                            } else if (anim % 40 > 10) {
                                sprite = Sprite.green_player_sprite_south_2;
                            } else {
                                sprite = Sprite.green_player_sprite_south_1;
                            }
                        }
                        break;
                    case WEST:
                        sprite = Sprite.green_player_sprite_west_0;
                        if (walking) {
                            if (anim % 40 > 35) {
                                sprite = Sprite.green_player_sprite_west_7;
                            } else if (anim % 40 > 30) {
                                sprite = Sprite.green_player_sprite_west_6;
                            } else if (anim % 40 > 25) {
                                sprite = Sprite.green_player_sprite_west_5;
                            } else if (anim % 40 > 20) {
                                sprite = Sprite.green_player_sprite_west_4;
                            } else if (anim % 40 > 15) {
                                sprite = Sprite.green_player_sprite_west_3;
                            } else if (anim % 40 > 10) {
                                sprite = Sprite.green_player_sprite_west_2;
                            } else {
                                sprite = Sprite.green_player_sprite_west_1;
                            }
                        }
                        break;
                    case EAST:
                        sprite = Sprite.green_player_sprite_east_0;
                        if (walking) {
                            if (anim % 40 > 35) {
                                sprite = Sprite.green_player_sprite_east_7;
                            } else if (anim % 40 > 30) {
                                sprite = Sprite.green_player_sprite_east_6;
                            } else if (anim % 40 > 25) {
                                sprite = Sprite.green_player_sprite_east_5;
                            } else if (anim % 40 > 20) {
                                sprite = Sprite.green_player_sprite_east_4;
                            } else if (anim % 40 > 15) {
                                sprite = Sprite.green_player_sprite_east_3;
                            } else if (anim % 40 > 10) {
                                sprite = Sprite.green_player_sprite_east_2;
                            } else {
                                sprite = Sprite.green_player_sprite_east_1;
                            }
                        }
                        break;
                    default:
                        sprite = Sprite.green_player_sprite_south_0;
                        break;
                }
            } else if (id == 3) {
                switch (direction) {
                    case NORTH:
                        sprite = Sprite.orange_player_sprite_north_0;
                        if (walking) {
                            if (anim % 40 > 35) {
                                sprite = Sprite.orange_player_sprite_north_7;
                            } else if (anim % 40 > 30) {
                                sprite = Sprite.orange_player_sprite_north_6;
                            } else if (anim % 40 > 25) {
                                sprite = Sprite.orange_player_sprite_north_5;
                            } else if (anim % 40 > 20) {
                                sprite = Sprite.orange_player_sprite_north_4;
                            } else if (anim % 40 > 15) {
                                sprite = Sprite.orange_player_sprite_north_3;
                            } else if (anim % 40 > 10) {
                                sprite = Sprite.orange_player_sprite_north_2;
                            } else {
                                sprite = Sprite.orange_player_sprite_north_1;
                            }
                        }
                        break;
                    case SOUTH:
                        sprite = Sprite.orange_player_sprite_south_0;
                        if (walking) {
                            if (anim % 40 > 35) {
                                sprite = Sprite.orange_player_sprite_south_7;
                            } else if (anim % 40 > 30) {
                                sprite = Sprite.orange_player_sprite_south_6;
                            } else if (anim % 40 > 25) {
                                sprite = Sprite.orange_player_sprite_south_5;
                            } else if (anim % 40 > 20) {
                                sprite = Sprite.orange_player_sprite_south_4;
                            } else if (anim % 40 > 15) {
                                sprite = Sprite.orange_player_sprite_south_3;
                            } else if (anim % 40 > 10) {
                                sprite = Sprite.orange_player_sprite_south_2;
                            } else {
                                sprite = Sprite.orange_player_sprite_south_1;
                            }
                        }
                        break;
                    case WEST:
                        sprite = Sprite.orange_player_sprite_west_0;
                        if (walking) {
                            if (anim % 40 > 35) {
                                sprite = Sprite.orange_player_sprite_west_7;
                            } else if (anim % 40 > 30) {
                                sprite = Sprite.orange_player_sprite_west_6;
                            } else if (anim % 40 > 25) {
                                sprite = Sprite.orange_player_sprite_west_5;
                            } else if (anim % 40 > 20) {
                                sprite = Sprite.orange_player_sprite_west_4;
                            } else if (anim % 40 > 15) {
                                sprite = Sprite.orange_player_sprite_west_3;
                            } else if (anim % 40 > 10) {
                                sprite = Sprite.orange_player_sprite_west_2;
                            } else {
                                sprite = Sprite.orange_player_sprite_west_1;
                            }
                        }
                        break;
                    case EAST:
                        sprite = Sprite.orange_player_sprite_east_0;
                        if (walking) {
                            if (anim % 40 > 35) {
                                sprite = Sprite.orange_player_sprite_east_7;
                            } else if (anim % 40 > 30) {
                                sprite = Sprite.orange_player_sprite_east_6;
                            } else if (anim % 40 > 25) {
                                sprite = Sprite.orange_player_sprite_east_5;
                            } else if (anim % 40 > 20) {
                                sprite = Sprite.orange_player_sprite_east_4;
                            } else if (anim % 40 > 15) {
                                sprite = Sprite.orange_player_sprite_east_3;
                            } else if (anim % 40 > 10) {
                                sprite = Sprite.orange_player_sprite_east_2;
                            } else {
                                sprite = Sprite.orange_player_sprite_east_1;
                            }
                        }
                        break;
                    default:
                        sprite = Sprite.orange_player_sprite_south_0;
                        break;
                }
            }
        }
        if (hitted) {
            if (rechargeTime % 20 > 10) {
                display.renderObject((int) x - (this.sprite.SIZE_BLOCK >> 1), (int) y - (this.sprite.SIZE_BLOCK >> 1), this.sprite);
            }
        } else {
            display.renderObject((int) x - (this.sprite.SIZE_BLOCK >> 1), (int) y - (this.sprite.SIZE_BLOCK >> 1), this.sprite);
        }
    }

    

    
   

}
