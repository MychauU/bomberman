/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.entity.mob;

import java.io.Serializable;

/**
 *
 * enum direction uzywana podczas ruchu
 */
public enum DIRECTION implements Serializable{
        
        NORTH(0),
        EAST(1),
        SOUTH(2),
        WEST(3);
        private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
        private final int direction;

        private DIRECTION(int dir) {
            this.direction = dir;
        }

        public int GETDIRECTION() {
            return direction;
        }
    }
