/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.entity.mob;

import java.io.Serializable;
import pl.bomberman.entity.Entity;

/**
 *
 * klasa mob
 * cos co sie porusza
 */

public abstract class Mob extends Entity implements Serializable{
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    protected int lives = 1;

    public int getLives() {
        return lives;
    }
    protected double speed = 1;

    public void decLives() {
        this.lives -= 1;

    }
    protected boolean immortal = false;
    protected boolean hitted = false;
    protected int anim = 0;
    protected boolean walking = false;
    protected DIRECTION direction = DIRECTION.SOUTH;
    protected boolean moving = false;

    
    public void setDirection(double xa,double ya){
        if (xa < 0) {
            direction = DIRECTION.WEST;
        } else if (xa > 0) {
            direction = DIRECTION.EAST;
        }
        if (ya < 0) {
            direction = DIRECTION.NORTH;
        } else if (ya > 0) {
            direction = DIRECTION.SOUTH;
        }
    }
    
    


    

}
