/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.entity;

import java.io.Serializable;
import pl.bomberman.graphics.Display;
import pl.bomberman.graphics.Sprite;
import pl.bomberman.level.Level;

/**
 *
 * klasa flame (ogien)
 */
public class Flame extends Entity implements Serializable{
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    private final int range;
    private int counter, anim;
    private final int maxCounter = 60;
    private int leftBound,rightBound,upBound,downBound;

    
    
    public Flame(double x, double y,int id, Level level) {
        this.x = x;
        this.y = y;
        this.id=id;
        this.range=0;
        this.sprite = Sprite.flame_sprite_0;
        counter = 0;
        this.level = level;
        this.blockSizeExp= level.getBlockSizeExp();
    }

    

    @Override
    public void render(Display display) {
        if (anim % maxCounter > 50) {
            sprite = Sprite.flame_sprite_5;
        } else if (anim % maxCounter > 40) {
            sprite = Sprite.flame_sprite_4;
        } else if (anim % maxCounter > 30) {
            sprite = Sprite.flame_sprite_3;
        } else if (anim % maxCounter > 20) {
            sprite = Sprite.flame_sprite_2;
        } else if (anim % maxCounter > 10) {
            sprite = Sprite.flame_sprite_1;
        }
        else {
            sprite = Sprite.flame_sprite_0;
        }
        for (int i=(int) (x-this.sprite.SIZE_BLOCK);i>=leftBound;i-=this.sprite.SIZE_BLOCK){
            display.renderObject(i - (this.sprite.SIZE_BLOCK >> 1), (int) (y - (this.sprite.SIZE_BLOCK >> 1)), this.sprite);
        }
        for (int i=(int) (x+this.sprite.SIZE_BLOCK);i<=rightBound;i+=this.sprite.SIZE_BLOCK){
            display.renderObject(i - (this.sprite.SIZE_BLOCK >> 1), (int) (y - (this.sprite.SIZE_BLOCK >> 1)), this.sprite);
        }
        for (int i=(int) (y-this.sprite.SIZE_BLOCK);i>=upBound;i-=this.sprite.SIZE_BLOCK){
            display.renderObject((int) (x - (this.sprite.SIZE_BLOCK >> 1)), i - (this.sprite.SIZE_BLOCK >> 1), this.sprite);
        }
        for (int i=(int) (y+this.sprite.SIZE_BLOCK);i<=downBound;i+=this.sprite.SIZE_BLOCK){
            display.renderObject((int) (x - (this.sprite.SIZE_BLOCK >> 1)), i - (this.sprite.SIZE_BLOCK >> 1), this.sprite);
        }
        display.renderObject((int)(x - (this.sprite.SIZE_BLOCK >> 1)), (int)(y - (this.sprite.SIZE_BLOCK >> 1)), this.sprite);

    }

    @Override
    public void update() {
        counter++;
        if (anim < 7500) {
            anim++;
        } else {
            anim = 0;
        }
        if (counter >= maxCounter) {
            remove();
            
        }
        
        
    }

    
    public int getLeftBound() {
        return leftBound;
    }

    public int getRightBound() {
        return rightBound;
    }

    public int getUpBound() {
        return upBound;
    }

    public int getDownBound() {
        return downBound;
    }
    
    public void setLeftBound(int i) {
        leftBound=i;
    }

    public void setRightBound(int i) {
         rightBound=i;
    }

    public void setDownBound(int i) {
         downBound=i;
    }

    public void setUpBound(int i) {
         upBound=i;
    }

}
