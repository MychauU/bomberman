/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.entity;


import java.io.Serializable;
import pl.bomberman.graphics.Display;
import pl.bomberman.graphics.Sprite;
import pl.bomberman.level.Level;

/**
 *
 * klasa bomba
 */
public class Bomb extends Entity implements Serializable{
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    private int anim;
    private int bombCounter;
    private final int range;
    private final int bombMaxCounter=180;
    
    
    public void detonate() {
        this.detonated = true;
    }
    
    
    
    //konstruktor sieciowy
    public Bomb(double x, double y, Level level, int range) {
        this.id=999;
        this.x = x;
        this.y = y;
        this.sprite = Sprite.bomb_sprite_0;
        this.blockSizeExp= level.getBlockSizeExp();
        this.range = range;
        anim = 0;
        bombCounter = 0;
        this.level = level;
    }
    

    @Override
    public void render(Display display) {
        if (anim % 40 > 20 && bombCounter>(bombMaxCounter>>1) ) {
            sprite = Sprite.bomb_sprite_3;
        } else if (anim % 40 <= 20 && bombCounter>(bombMaxCounter>>1)) {
            sprite = Sprite.bomb_sprite_2;
        } else if (anim % 40 > 20 && bombCounter<=(bombMaxCounter>>1)) {
            sprite = Sprite.bomb_sprite_1;
        } else {
            sprite = Sprite.bomb_sprite_0;
        }
        display.renderObject((int)x - (this.sprite.SIZE_BLOCK >> 1), (int)y - (this.sprite.SIZE_BLOCK >>1), this.sprite);
    }

    @Override
    public void update() {
        if (anim < 7500) {
            anim++;
        } else {
            anim = 0;
        }
        if (bombCounter > bombMaxCounter || detonated) {
            remove();
        }
        bombCounter++;
    }
  
    
    

}
