/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.entity;

import java.io.Serializable;
import pl.bomberman.graphics.Display;
import pl.bomberman.graphics.Sprite;
import pl.bomberman.level.Level;

/**
 *
 * klasa typu box (skrzynki)
 */
public class Box extends Entity implements Serializable {
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    private int counter = 0;
    private final int maxCounter = 60;
    

    public Box(int x, int y, Sprite sprite, Level level) {
        this.x = x;
        this.y = y;
        this.sprite = sprite;
        this.level = level;
        this.blockSizeExp = level.getBlockSizeExp();  
        
    }

    @Override
    public void render(Display display) {
        display.renderObject((int) x - (this.sprite.SIZE_BLOCK >> 1), (int) y - (this.sprite.SIZE_BLOCK >> 1), sprite);
    }

    @Override
    public void update() {
        if (detonated) {
            if (counter >= maxCounter) {
                this.remove();
            } else {
                counter++;
            }
        }
    }

    public void detonate() {
        detonated = true;
        counter = 0;

    }

   

}
