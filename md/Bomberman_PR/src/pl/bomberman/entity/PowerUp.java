/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.entity;

import java.awt.geom.Point2D;
import java.io.Serializable;
import pl.bomberman.entity.mob.Player;
import pl.bomberman.graphics.Display;
import pl.bomberman.graphics.Sprite;
import pl.bomberman.level.Level;
import pl.bomberman.sendpack.MESSAGETYPE;
import pl.bomberman.sendpack.MessagePackage;

/**
 *
 * klasa powerup(nagrody)
 */
public class PowerUp extends Entity implements Serializable{
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    private final UPGRADE upgrade;
    
    public PowerUp(double x,double y,Level level,UPGRADE upgrade){
        this.upgrade=upgrade;
        this.id=upgrade.GETUPGRADE();
        this.x = x;
        this.y = y;
        this.blockSizeExp=level.getBlockSizeExp();
        this.level = level;
        chooseSprite();
    }
    
    @Override
    public void render(Display display) {
        display.renderObject((int)x - (this.sprite.SIZE_BLOCK >> 1),(int) y - (this.sprite.SIZE_BLOCK >>1), this.sprite);
    }

    @Override
    public void update() {

    }

    private void chooseSprite() {
        if (null!=upgrade)switch (upgrade) {
            case POWER_UP:
                sprite=Sprite.power_up;
                break;
            case HEALTH_UP:
                sprite=Sprite.health_up;
                break;
            case AMMO_UP:
                sprite=Sprite.ammo_up;
                break;
            case SPEED_UP:
                sprite=Sprite.speed_up;
                break;
            default:
                System.out.println("couldnt find upgrade");
                break;
        }
    }
    
    
    public void powerUpPlayer(Player player) {
        
        if (null != upgrade) {
            switch (upgrade) {
                case POWER_UP:
                    player.incBombRange();
                    break;
                case HEALTH_UP:
                    player.incLives();
                    break;
                case AMMO_UP:
                    player.incMaxBombs();
                    player.incBombsInHand();
                    break;
                case SPEED_UP:
                    player.incSpeed();
                    break;
                default:
                    System.out.println("error nie ulepszono playera");
                    break;
            }
        }
        
    }

   


}
