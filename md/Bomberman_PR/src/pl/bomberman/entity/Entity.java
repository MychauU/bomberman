/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.entity;

import java.io.Serializable;
import java.util.Random;
import pl.bomberman.graphics.Display;
import pl.bomberman.graphics.Sprite;
import pl.bomberman.level.Level;

/**
 *
 * abstrakcyjna klasa entity
 */
//cos co zyje
public abstract class Entity implements Serializable{
    public double x,y;
    protected int id=0;
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id= id;
    }
    protected boolean removed=false;
    protected Level level;
    protected final Random random=new Random();
    protected Sprite sprite;
    protected int blockSizeExp;
    protected boolean detonated=false;
    public abstract void render(Display display);
    
    public abstract void update();
    
    public void remove(){
        removed=true;
    }
    
    public boolean isRemoved(){
        return removed;
    }
    
    
    
}
