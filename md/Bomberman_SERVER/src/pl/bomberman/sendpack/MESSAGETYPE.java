/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.sendpack;

import java.io.Serializable;

/**
 *
 * @author MychauU
 */
public enum MESSAGETYPE implements Serializable{
    MOVEMENTFROMCLIENT(0),
    CREATEBOMBFROMCLIENT(1),
    CREATEFIREFROMSERVER(2),
    CREATEPOWERUPFROMSERVER(3),
    REMOVEBOXFROMSERVER(4),
    REMOVEPOWERUPFROMSERVER(5),
    DECLIFE(6),
    REMOVEPLAYER(7),
    SENDLEVEL(8),
    IDENTIFY(9),
    DELETEBOMBFROMSERVER(10),
    MOVEMENTFROMSERVER(11),
    CREATEBOMBFROMSERVER(12),
    SENDPLAYERSFROMSERVER(13),
    SENDBOXESFROMSERVER(14),
    SENDPLAYERNAMEFROMCLIENT(15),
    SENDPLAYERSNAMEFROMSERVER(16),
    STARTGAME(333),
    CREATENEWGAME(444),
    WINNERPLAYERFROMSERVER(609),
    DISCONNECTEDPLAYERFROMSERVER(888),
    STOPGAME(666);
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    private int message;

    private MESSAGETYPE(int dir) {
        this.message = dir;
    }

    public int GETMESSAGETYPE() {
        return message;
    }
}
