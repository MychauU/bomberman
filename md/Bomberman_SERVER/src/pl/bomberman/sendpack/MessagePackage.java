/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.sendpack;

import java.io.Serializable;

/**
 *
 * @author MychauU
 */
public class MessagePackage implements Serializable {
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    public Object obj;
    public MESSAGETYPE message;
    public int id;
    public Object additionalInformation;
    public MessagePackage(){
        obj=null;
        message=null;
        id=999;
        additionalInformation=999;
    }
    
    public MessagePackage(Object obj,MESSAGETYPE message,int id){
        this.obj=obj;
        this.message=message;
        this.id=id;
    }
    public MessagePackage(Object obj,MESSAGETYPE message){
        this.obj=obj;
        this.message=message;
        this.id=999;
    }
}
