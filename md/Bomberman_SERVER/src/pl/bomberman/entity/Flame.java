/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.entity;

import java.io.Serializable;
import java.util.Iterator;
import pl.bomberman.StaticFunctions;
import pl.bomberman.entity.mob.Player;

import pl.bomberman.graphics.Sprite;
import pl.bomberman.level.Level;

/**
 *
 * @author MychauU
 */
public class Flame extends Entity implements Serializable {

    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    private int range;
    private int counter, anim;
    private final int maxCounter = 60;
    public int leftBound, rightBound, upBound, downBound;

    Flame(double x, double y, int range, Level level) {
        this.x = x;
        this.y = y;
        this.range = range;
        this.sprite = Sprite.flame_sprite_0;
        counter = 0;
        this.id = 999;
        this.level = level;
        this.blockSizeExp = level.getBlockSizeExp();
        setBounds();
        detonateBombs();
    }

    Flame(double x, double y, int id, int range, Level level) {
        this.x = x;
        this.y = y;
        this.id = id;
        this.range = range;
        this.sprite = Sprite.flame_sprite_0;
        counter = 0;
        this.level = level;
        this.blockSizeExp = level.getBlockSizeExp();
        setBounds();
        detonateBombs();
    }

    public void flameCollision(int xa, int ya) {
//        synchronized (Level.bombs) {
//            Iterator i = Level.bombs.iterator(); // Must be in synchronized block
//            while (i.hasNext()) {
//                Bomb pom = (Bomb) i.next();
//                if (StaticFunctions.collision(pom.x, pom.y, xa, ya)) {
//                    pom.detonate();
//                }
//            }
//        }
        synchronized (Level.players) {
            Iterator i = Level.players.iterator(); // Must be in synchronized block
            while (i.hasNext()) {
                Player pom = (Player) i.next();
                if (StaticFunctions.collision(pom.x, pom.y, xa, ya)) {
                    pom.decLives();
                }
            }
        }
        
    }

    @Override
    public void update() {

        if (anim < 7500) {
            anim++;
        } else {
            anim = 0;
        }
        if (counter >= maxCounter) {
            remove();
            if (this.id != 999) {
                for (Player x : level.players) {
                    if (x.getId() == id) {
                        x.incBombsInHand();
                    }
                }
            }
        }
        if (this.isRemoved()) {
            return;
        }
        for (int i = (int) (x - this.sprite.SIZE_BLOCK); i >= leftBound; i -= this.sprite.SIZE_BLOCK) {
            flameCollision(i, (int) this.y);
        }
        for (int i = (int) (x + this.sprite.SIZE_BLOCK); i <= rightBound; i += this.sprite.SIZE_BLOCK) {
            flameCollision(i, (int) this.y);
        }
        for (int i = (int) (y - this.sprite.SIZE_BLOCK); i >= upBound; i -= this.sprite.SIZE_BLOCK) {
            flameCollision((int) this.x, i);
        }
        for (int i = (int) (y + this.sprite.SIZE_BLOCK); i <= downBound; i += this.sprite.SIZE_BLOCK) {
            flameCollision((int) this.x, i);
        }
        flameCollision((int) this.x, (int) this.y);
        counter++;
    }

    private void setBounds() {
        boolean changed = false;
        for (int i = (int) (x - this.sprite.SIZE_BLOCK); i >= x - (range << this.blockSizeExp); i -= this.sprite.SIZE_BLOCK) {
            if (tileCollision(i, (int) this.y)) {
                leftBound = i + this.sprite.SIZE_BLOCK;
                changed = true;
                break;
            }
            if (boxCollision(i, (int) this.y)) {
                leftBound = i;
                changed = true;
                break;
            }
        }
        if (!changed) {
            leftBound = (int) x - (range << this.blockSizeExp);
        }
        changed = false;

        for (int i = (int) (x + this.sprite.SIZE_BLOCK); i <= x + (range << this.blockSizeExp); i += this.sprite.SIZE_BLOCK) {
            if (tileCollision(i, (int) this.y)) {
                rightBound = i - this.sprite.SIZE_BLOCK;
                changed = true;
                break;
            }
            if (boxCollision(i, (int) this.y)) {
                rightBound = i;
                changed = true;
                break;
            }
        }
        if (!changed) {
            rightBound = (int) (x + (range << this.blockSizeExp));
        }
        changed = false;
        for (int i = (int) (y - this.sprite.SIZE_BLOCK); i >= y - (range << this.blockSizeExp); i -= this.sprite.SIZE_BLOCK) {
            if (tileCollision((int) this.x, i)) {
                upBound = i + this.sprite.SIZE_BLOCK;
                changed = true;
                break;
            }
            if (boxCollision((int) this.x, i)) {
                upBound = i;
                changed = true;
                break;
            }
        }
        if (!changed) {
            upBound = (int) y - (range << this.blockSizeExp);
        }
        changed = false;
        for (int i = (int) (y + this.sprite.SIZE_BLOCK); i <= y + (range << this.blockSizeExp); i += this.sprite.SIZE_BLOCK) {
            if (tileCollision((int) this.x, i)) {
                downBound = i - this.sprite.SIZE_BLOCK;
                changed = true;
                break;
            }
            if (boxCollision((int) this.x, i)) {
                downBound = i;
                changed = true;
                break;
            }
        }
        if (!changed) {
            downBound = (int) y + (range << this.blockSizeExp);

        }
        changed = false;
    }

    //mozna zredukowac do porownywania x i y z boxami zamiast sprawdzac czy element jest w srodku
    private boolean boxCollision(int xa, int ya) {
        synchronized (Level.boxes) {
            Iterator i = Level.boxes.iterator(); // Must be in synchronized block
            while (i.hasNext()) {
                Box pom = (Box) i.next();
                if (StaticFunctions.collision(pom.x, pom.y, xa, ya)) {
                    pom.detonate();
                    return true;
                }
            }
            
        }
        return false;
    }

    //funkcja sprawdza czy powinienem dodac ogien sprawdza mape 
    private boolean tileCollision(int xa, int ya) {
        //     break; //? czy powinienem
        return level.getTile(xa >> blockSizeExp, ya >> blockSizeExp).isSolid();
    }

    private void detonateBombs() {
        for (int i = (int) (x - this.sprite.SIZE_BLOCK); i >= leftBound; i -= this.sprite.SIZE_BLOCK) {
            bombCollision(i, (int) this.y);
        }
        for (int i = (int) (x + this.sprite.SIZE_BLOCK); i <= rightBound; i += this.sprite.SIZE_BLOCK) {
            bombCollision(i, (int) this.y);
        }
        for (int i = (int) (y - this.sprite.SIZE_BLOCK); i >= upBound; i -= this.sprite.SIZE_BLOCK) {
            bombCollision((int) this.x, i);
        }
        for (int i = (int) (y + this.sprite.SIZE_BLOCK); i <= downBound; i += this.sprite.SIZE_BLOCK) {
            bombCollision((int) this.x, i);
        }
        
        
        

    }
    
    private void bombCollision(int xa, int ya) {
        synchronized (Level.bombs) {
            Iterator i = Level.bombs.iterator(); // Must be in synchronized block
            while (i.hasNext()) {
                Bomb pom = (Bomb) i.next();
                if (StaticFunctions.collision(pom.x, pom.y, xa, ya)) {
                    pom.detonate();
                }
            }
        }
    
    }
    


}
