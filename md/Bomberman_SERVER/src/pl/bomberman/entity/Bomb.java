/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.entity;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import static pl.bomberman.StaticFunctions.isInsideSquare;
import pl.bomberman.entity.mob.Player;
import pl.bomberman.graphics.Sprite;
import pl.bomberman.level.Level;
import pl.bomberman.sendpack.MESSAGETYPE;
import pl.bomberman.sendpack.MessagePackage;
import pl.bomberman.server.Server;

/**
 *
 * @author MychauU
 */
public class Bomb extends Entity implements Serializable {

    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    private int anim;
    private int bombCounter;
    private final int range;
    private boolean detonated = false;
    private final int bombMaxCounter = 180;
    public List<Integer> pplWhoCanStepOnMe;

    public Bomb(double x, double y, Level level, int range, int id) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.sprite = Sprite.bomb_sprite_0;
        this.blockSizeExp = level.getBlockSizeExp();
        this.range = range;
        anim = 0;
        bombCounter = 0;
        this.level = level;
        pplWhoCanStepOnMe = new ArrayList<>();
        addPplWhoCanStepOnMe();
    }

    @Override
    public void update() {
        if (anim < 7500) {
            anim++;
        } else {
            anim = 0;
        }
        if (bombCounter > bombMaxCounter || detonated) {
            //send a message
            Flame pomFire = new Flame(x, y, id, range, level);
            int tab[] = new int[4];
            tab[0] = pomFire.leftBound;
            tab[1] = pomFire.upBound;
            tab[2] = pomFire.rightBound;
            tab[3] = pomFire.downBound;
            MessagePackage pom = new MessagePackage(new Point2D.Double(this.x, this.y), MESSAGETYPE.CREATEFIREFROMSERVER, this.id);
            pom.additionalInformation = tab;
            Level.flames.add(pomFire);
            Server.messages.add(pom);
            remove();
        }
        checkPplWhoCanStepOnMe();
        bombCounter++;
    }

    public void detonate() {
        if (!this.detonated) {
            //send a message that bomb should be deleted earlier
            MessagePackage pom = new MessagePackage(new Point2D.Double(this.x, this.y), MESSAGETYPE.DELETEBOMBFROMSERVER, this.id);
            Server.messages.add(pom);
            this.detonated = true;
        }

    }

    private void checkPplWhoCanStepOnMe() {
        double xa, ya;
        Iterator<Integer> j;
        j = pplWhoCanStepOnMe.iterator();
        while (j.hasNext()) {
            Integer s = j.next(); // must be called before you can call i.remove()
            // Do something
            synchronized (Level.players) {
                Iterator i = Level.players.iterator(); // Must be in synchronized block
                while (i.hasNext()) {
                    Player pom = (Player) i.next();
                    if (s == pom.id) {
                        xa = pom.x;
                        ya = pom.y;
                        if (!playerCollision(xa, ya)) {
                            j.remove();
                            break;
                        }
                    }

                }
            }
        }
    }

    private void addPplWhoCanStepOnMe() {
        double xa, ya;
        synchronized (Level.players) {
            Iterator i = Level.players.iterator(); // Must be in synchronized block
            while (i.hasNext()) {
                Player pom = (Player) i.next();
                xa = pom.x;
                ya = pom.y;
                if (playerCollision(xa, ya)) {
                    this.pplWhoCanStepOnMe.add(pom.id);
                }
            }
        }
    }

    private boolean playerCollision(double xa, double ya) {
        int xt, yt;
        Point P = new Point();
        Point xy1 = new Point();
        Point xy2 = new Point();
        Point xy3 = new Point();
        Point xy4 = new Point();
        double xx, yy;
        int ix, iy;
        for (int c = 0; c < 4; c++) {
            xx = ((xa) + c % 2 * (sprite.SIZE_BLOCK >> 1) - 8);
            yy = ((ya) + c / 2 * (sprite.SIZE_BLOCK >> 1) - 4);
            ix = (int) Math.ceil(xx);
            iy = (int) Math.ceil(yy);
            if (c % 2 == 0) {
                ix = (int) Math.floor(xx);
            }
            if (c / 2 == 0) {
                iy = (int) Math.floor(yy);
            }
            P.x = ix;
            P.y = iy;
            xt = (int) this.x;
            yt = (int) this.y;
            xy1.x = xt - (sprite.SIZE_BLOCK >> 1) + 1;
            xy1.y = yt - (sprite.SIZE_BLOCK >> 1) + -1;
            xy2.x = xt + (sprite.SIZE_BLOCK >> 1) - 1;
            xy2.y = yt - (sprite.SIZE_BLOCK >> 1) + 1;
            xy3.x = xt + (sprite.SIZE_BLOCK >> 1) - 1;
            xy3.y = yt + (sprite.SIZE_BLOCK >> 1) - 1;
            xy4.x = xt - (sprite.SIZE_BLOCK >> 1) + 1;
            xy4.y = yt + (sprite.SIZE_BLOCK >> 1) - 1;
            if (isInsideSquare(xy1, xy2, xy3, xy4, P)) {
                return true;
            }
        }
        return false;
    }

}
