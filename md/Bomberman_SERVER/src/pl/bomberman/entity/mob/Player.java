/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.entity.mob;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.io.Serializable;
import static pl.bomberman.StaticFunctions.isInsideSquare;
import pl.bomberman.entity.Bomb;
import pl.bomberman.graphics.Sprite;
import pl.bomberman.input.Keyboard;
import pl.bomberman.level.Level;
import pl.bomberman.sendpack.MESSAGETYPE;
import pl.bomberman.sendpack.MessagePackage;
import pl.bomberman.server.Server;

/**
 *
 * @author MychauU
 */
public class Player extends Mob implements Serializable {

    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    private Keyboard keyboard;
    private int bombsInHand = 1;

    private boolean disconnected=false;
    
    public void setDisconnected(boolean pom){
        this.disconnected=pom;
    }
    
    public boolean getDisconnected( ){
        return this.disconnected;
    }
    
    public int getBombsInHand() {
        return bombsInHand;
    }
    private int maxBombs = 1;

    public int getMaxBombs() {
        return maxBombs;
    }

    int rechargeTime = 0;
    private boolean alive = true;
    private int bombRange = 3;
    private String name;
    public int getBombRange() {
        return bombRange;
    }
    private boolean dying = false;

    public void incMaxBombs() {
        this.maxBombs += 1;
    }

    public void incBombsInHand() {
        this.bombsInHand += 1;
    }

    public void decBombsInHand() {
        this.bombsInHand -= 1;
    }

    public void incLives() {
        this.lives += 1;
    }

    public void incBombRange() {
        this.bombRange += 1;
    }

    public void incSpeed() {
        if (this.speed < 6) {
            this.speed *= 1.5;
            if (this.speed > 6) {
                this.speed = 6;
            }
        }
    }

    public Point2D.Double getCooridinates() {
        return new Point2D.Double(this.x, this.y);
    }

    public void setCooridinates(Point2D.Double xy) {
        this.x = xy.x;
        this.y = xy.y;
    }

    public Player(double x, double y, int id, Keyboard input, Level level) {
        this.lives = 2;
        this.level = level;
        this.id = id;
        this.keyboard = input;
        this.x = x;
        this.y = y;
        switch (id) {
            case 1:
                sprite = Sprite.red_player_sprite_south_0;
                break;
            case 2:
                sprite = Sprite.blue_player_sprite_south_0;
                break;
            default:
                sprite = Sprite.red_player_sprite_south_0;
                break;
        }
        this.blockSizeExp = level.getBlockSizeExp();
    }

    //konstruktor na siecowych playerow
    public Player(double x, double y, int id, Level level) {
        this.lives = 2;
        this.level = level;
        this.id = id;
        this.keyboard = null;
        this.x = x;
        this.y = y;
        sprite = Sprite.red_player_sprite_south_0;
        this.blockSizeExp = level.getBlockSizeExp();
    }

    @Override
    public void decLives() {
        if (!immortal) {
            super.decLives();
            hitted = true;
            immortal = true;
            rechargeTime = 240;
            MessagePackage pom = new MessagePackage(null, MESSAGETYPE.DECLIFE, this.id);
            Server.messages.add(pom);
        }

    }

    @Override
    public void update() {
        if (dying) {
            if (rechargeTime < 0) {
                remove();
                return;
            } else {
                rechargeTime--;
                return;
            }
        }
        if (immortal) {
            if (rechargeTime < 0) {
                immortal = false;
                hitted = false;
            } else {
                rechargeTime--;
            }

        }
        if (lives <= 0) {
            dying = true;
            rechargeTime = 120;
            
        //    System.out.println("Zgineles");
            return;
        }

        double xa = 0;
        double ya = 0;
        if (anim < 7500) {
            anim++;
        } else {
            anim = 0;
        }

    }
    
    

    //sprawdza czy istnieje juz tam bomba
    private boolean checkIfSpotAvailable() {
        int xt = (((int) this.x >> this.blockSizeExp) << this.blockSizeExp);
        xt += sprite.SIZE_BLOCK >> 1;
        int yt = (((int) this.y >> this.blockSizeExp) << this.blockSizeExp);
        yt += sprite.SIZE_BLOCK >> 1;
        return !bombCollision(xt, yt);
    }

    private void plantBomb() {
        int xt = (((int) this.x >> this.blockSizeExp) << this.blockSizeExp);
        xt += sprite.SIZE_BLOCK >> 1;
        int yt = (((int) this.y >> this.blockSizeExp) << this.blockSizeExp);
        yt += sprite.SIZE_BLOCK >> 1;
        level.bombs.add(new Bomb(xt, yt, level, bombRange, this.id));
    }

    private boolean bombCollision(int xa, int ya) {
        int xt, yt;
        Point P = new Point();
        Point xy1 = new Point();
        Point xy2 = new Point();
        Point xy3 = new Point();
        Point xy4 = new Point();
        synchronized (level.bombs) {
            for (int i = 0; i < level.bombs.size(); i++) {
                P.x = xa;
                P.y = ya;
                xt = (int) level.bombs.get(i).x;
                yt = (int) level.bombs.get(i).y;
                xy1.x = xt - (sprite.SIZE_BLOCK >> 1) + 4;
                xy1.y = yt - (sprite.SIZE_BLOCK >> 1) + 4;
                xy2.x = xt + (sprite.SIZE_BLOCK >> 1) - 4;
                xy2.y = yt - (sprite.SIZE_BLOCK >> 1) + 4;
                xy3.x = xt + (sprite.SIZE_BLOCK >> 1) - 4;
                xy3.y = yt + (sprite.SIZE_BLOCK >> 1) - 4;
                xy4.x = xt - (sprite.SIZE_BLOCK >> 1) + 4;
                xy4.y = yt + (sprite.SIZE_BLOCK >> 1) - 4;
                if (isInsideSquare(xy1, xy2, xy3, xy4, P)) {
                    return true;
                }
            }
            return false;
        }
    }

}
