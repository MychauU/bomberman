/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.entity.mob;

import java.awt.Point;
import java.io.Serializable;
import static pl.bomberman.StaticFunctions.isInsideSquare;
import pl.bomberman.entity.Entity;

/**
 *
 * @author MychauU
 */
//cos co sie porusza
public abstract class Mob extends Entity implements Serializable {

    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    protected int lives = 1;

    public int getLives() {
        return lives;
    }
    protected double speed = 1;

    public void decLives() {
        this.lives -= 1;

    }
    protected boolean immortal = false;
    protected boolean hitted = false;
    protected int anim = 0;
    protected boolean walking = false;
    protected DIRECTION direction = DIRECTION.SOUTH;
    protected boolean moving = false;

    public DIRECTION setDirection(double xa, double ya) {
        if (xa < 0) {
            direction = DIRECTION.WEST;
        } else if (xa > 0) {
            direction = DIRECTION.EAST;
        }
        if (ya < 0) {
            direction = DIRECTION.NORTH;
        } else if (ya > 0) {
            direction = DIRECTION.SOUTH;
        }
        return direction;
    }

    public void move(double xa, double ya) {
        if (xa != 0 && ya != 0) {
            move(xa, 0);
            move(0, ya);
            return;
        }

        //nowe
        xa *= speed;
        ya *= speed;
        //

        while (xa != 0) {
            if (Math.abs(xa) > 1) {
                if (!collision(abs(xa), ya) && !boxesCollision(abs(xa), ya) && !bombsCollision(abs(xa), ya)) {
                    this.x += abs(xa);
                }
                xa -= abs(xa);
            } else {
                if (!collision(abs(xa), ya) && !boxesCollision(abs(xa), ya) && !bombsCollision(abs(xa), ya)) {
                    this.x += xa;
                }
                xa = 0;
            }
        }
        while (ya != 0) {
            if (Math.abs(ya) > 1) {
                if (!collision(xa, abs(ya)) && !boxesCollision(xa, abs(ya)) && !bombsCollision(xa, abs(ya))) {
                    this.y += abs(ya);
                }
                ya -= abs(ya);
            } else {
                if (!collision(xa, abs(ya)) && !boxesCollision(xa, abs(ya)) && !bombsCollision(xa, abs(ya))) {
                    this.y += abs(ya);
                }
                ya = 0;
            }
        }
    }

    private int abs(double value) {
        if (value < 0) {
            return -1;
        } else {
            return 1;
        }
    }

    private boolean collision(double xa, double ya) {
        double xt, yt;
        int ix, iy;
        for (int c = 0; c < 4; c++) {
            xt = ((x + xa) - c % 2 * (sprite.SIZE_BLOCK >> 1) - 8) / sprite.SIZE_BLOCK;
            yt = ((y + ya) - c / 2 * (sprite.SIZE_BLOCK >> 1) - 4) / sprite.SIZE_BLOCK;
            ix = (int) Math.ceil(xt);
            iy = (int) Math.ceil(yt);
            if (c % 2 == 0) {
                ix = (int) Math.floor(xt);
            }
            if (c / 2 == 0) {
                iy = (int) Math.floor(yt);
            }
            if (level.getTile(ix, iy).isSolid()) {
                return true;
            }
        }
        return false;
    }

    private boolean bombsCollision(double xa, double ya) {
        int xt, yt;
        Point P = new Point();
        Point xy1 = new Point();
        Point xy2 = new Point();
        Point xy3 = new Point();
        Point xy4 = new Point();
        int ix, iy;
        double xx, yy;
        synchronized (level.bombs) {
            for (int i = 0; i < level.bombs.size(); i++) {
                for (Integer pplWhoCanStepOnMe : level.bombs.get(i).pplWhoCanStepOnMe) {
                    if (pplWhoCanStepOnMe == this.id) {
                        return false;
                    }
                }
                for (int c = 0; c < 4; c++) {
                    xx = ((this.x + xa) + c % 2 * (sprite.SIZE_BLOCK >> 1) - 8);
                    yy = ((this.y + ya) + c / 2 * (sprite.SIZE_BLOCK >> 1) - 4);
                    ix = (int) Math.ceil(xx);
                    iy = (int) Math.ceil(yy);
                    if (c % 2 == 0) {
                        ix = (int) Math.floor(xx);
                    }
                    if (c / 2 == 0) {
                        iy = (int) Math.floor(yy);
                    }
                    P.x = ix;
                    P.y = iy;
                    xt = (int) level.bombs.get(i).x;
                    yt = (int) level.bombs.get(i).y;
                    xy1.x = xt - (sprite.SIZE_BLOCK >> 1) + 1;
                    xy1.y = yt - (sprite.SIZE_BLOCK >> 1) + 1;
                    xy2.x = xt + (sprite.SIZE_BLOCK >> 1) - 1;
                    xy2.y = yt - (sprite.SIZE_BLOCK >> 1) + 1;
                    xy3.x = xt + (sprite.SIZE_BLOCK >> 1) - 1;
                    xy3.y = yt + (sprite.SIZE_BLOCK >> 1) - 1;
                    xy4.x = xt - (sprite.SIZE_BLOCK >> 1) + 1;
                    xy4.y = yt + (sprite.SIZE_BLOCK >> 1) - 1;
                    if (isInsideSquare(xy1, xy2, xy3, xy4, P)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean boxesCollision(double xa, double ya) {
        int xt, yt;
        Point P = new Point();
        Point xy1 = new Point();
        Point xy2 = new Point();
        Point xy3 = new Point();
        Point xy4 = new Point();
        int ix, iy;
        double xx, yy;
        synchronized (level.boxes) {
            for (int i = 0; i < level.boxes.size(); i++) {
                for (int c = 0; c < 4; c++) {
                    xx = ((this.x + xa) + c % 2 * (sprite.SIZE_BLOCK >> 1) - 8);
                    yy = ((this.y + ya) + c / 2 * (sprite.SIZE_BLOCK >> 1) - 4);
                    ix = (int) Math.ceil(xx);
                    iy = (int) Math.ceil(yy);
                    if (c % 2 == 0) {
                        ix = (int) Math.floor(xx);
                    }
                    if (c / 2 == 0) {
                        iy = (int) Math.floor(yy);
                    }
                    P.x = ix;
                    P.y = iy;
                    xt = (int) level.boxes.get(i).x;
                    yt = (int) level.boxes.get(i).y;
                    xy1.x = xt - (sprite.SIZE_BLOCK >> 1) + 1;
                    xy1.y = yt - (sprite.SIZE_BLOCK >> 1) + 1;
                    xy2.x = xt + (sprite.SIZE_BLOCK >> 1) - 1;
                    xy2.y = yt - (sprite.SIZE_BLOCK >> 1) + 1;
                    xy3.x = xt + (sprite.SIZE_BLOCK >> 1) - 1;
                    xy3.y = yt + (sprite.SIZE_BLOCK >> 1) - 1;
                    xy4.x = xt - (sprite.SIZE_BLOCK >> 1) + 1;
                    xy4.y = yt + (sprite.SIZE_BLOCK >> 1) - 1;
                    if (isInsideSquare(xy1, xy2, xy3, xy4, P)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
