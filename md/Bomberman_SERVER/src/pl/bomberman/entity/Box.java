/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.entity;

import java.awt.geom.Point2D;
import java.io.Serializable;

import pl.bomberman.graphics.Sprite;
import pl.bomberman.level.Level;
import pl.bomberman.sendpack.MESSAGETYPE;
import pl.bomberman.sendpack.MessagePackage;
import pl.bomberman.server.Server;

/**
 *
 * @author MychauU
 */
public class Box extends Entity implements Serializable {

    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    private boolean detonated = false;
    private int counter = 0;
    private final int maxCounter = 60;

    public Box(int x, int y, Sprite sprite, Level level) {
        this.x = x;
        this.y = y;
        this.sprite = sprite;
        this.level = level;
        this.blockSizeExp = level.getBlockSizeExp();

    }

    @Override
    public void update() {
        if (detonated) {
            if (counter >= maxCounter) {
                //tutaj rand na power upa
                if (random.nextInt(4) < 2) {
                    createPowerUp();
                }
                this.remove();
            } else {
                counter++;
            }
        }
    }

    void detonate() {
        if (!detonated) {
            detonated = true;
            MessagePackage pom = new MessagePackage(new Point2D.Double(this.x, this.y), MESSAGETYPE.REMOVEBOXFROMSERVER, 999);
            Server.messages.add(pom);
            counter = 0;
        }

    }

    private void createPowerUp() {
        int power = random.nextInt(5);
        UPGRADE pom;
        switch (power) {
            case 0:
                pom = UPGRADE.POWER_UP;
                break;
            case 1:
                pom = UPGRADE.HEALTH_UP;
                break;
            case 2:
                pom = UPGRADE.AMMO_UP;
                break;
            case 3:
                pom = UPGRADE.SPEED_UP;
                break;
            default:
                pom = null;
                break;
        }
        if (pom != null) {
            PowerUp newPowerUp = new PowerUp(this.x, this.y, level, pom);
            Level.powerups.add(newPowerUp);
            MessagePackage msg = new MessagePackage(new Point2D.Double(this.x, this.y), MESSAGETYPE.CREATEPOWERUPFROMSERVER, 999);
            msg.additionalInformation = pom;
            Server.messages.add(msg);
        }
    }

}
