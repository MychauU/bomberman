/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.entity;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.Iterator;
import pl.bomberman.StaticFunctions;
import static pl.bomberman.StaticFunctions.isInsideSquare;
import pl.bomberman.entity.mob.Player;
import pl.bomberman.graphics.Sprite;
import pl.bomberman.level.Level;
import pl.bomberman.sendpack.MESSAGETYPE;
import pl.bomberman.sendpack.MessagePackage;
import pl.bomberman.server.Server;

/**
 *
 * @author MychauU
 */
public class PowerUp extends Entity implements Serializable {

    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html

    private UPGRADE upgrade;

    public PowerUp(double x, double y, Level level, UPGRADE upgrade) {
        this.upgrade = upgrade;
        this.id = upgrade.GETUPGRADE();
        this.x = x;
        this.y = y;
        this.blockSizeExp = level.getBlockSizeExp();
        this.level = level;
        chooseSprite();
    }

    @Override
    public void update() {
        if (isRemoved()) {
            return;
        }
        if (playersCollision()) {
            remove();
        }
    }

    private void chooseSprite() {
        if (null != upgrade) {
            switch (upgrade) {
                case POWER_UP:
                    sprite = Sprite.power_up;
                    break;
                case HEALTH_UP:
                    sprite = Sprite.health_up;
                    break;
                case AMMO_UP:
                    sprite = Sprite.ammo_up;
                    break;
                case SPEED_UP:
                    sprite = Sprite.speed_up;
                    break;
                default:
                    System.out.println("error nie znaleziono upgrade");
                    break;
            }
        }
    }

    private boolean playersCollision() {
        int xt, yt;
        Point P = new Point();
        Point xy1 = new Point();
        Point xy2 = new Point();
        Point xy3 = new Point();
        Point xy4 = new Point();
        synchronized (Level.players) {
            Iterator i = Level.players.iterator(); // Must be in synchronized block
            while (i.hasNext()) {
                Player pom = (Player) i.next();
                if (StaticFunctions.collision(pom.x, pom.y, this.x, this.y)) {
                    powerUpPlayer(pom);
                    return true;
                }
            }


        }
        return false;
    }

    private void powerUpPlayer(Player player) {
        
        if (null != upgrade) {
            switch (upgrade) {
                case POWER_UP:
                    player.incBombRange();
                    break;
                case HEALTH_UP:
                    player.incLives();
                    break;
                case AMMO_UP:
                    player.incMaxBombs();
                    player.incBombsInHand();
                    break;
                case SPEED_UP:
                    player.incSpeed();
                    break;
                default:
                    System.out.println("error nie ulepszono playera");
                    break;
            }
        }
        MessagePackage obj = new MessagePackage(new Point2D.Double(this.x,this.y), MESSAGETYPE.REMOVEPOWERUPFROMSERVER, player.id);
        obj.additionalInformation = upgrade;
        Server.messages.add(obj);
        
    }

}
