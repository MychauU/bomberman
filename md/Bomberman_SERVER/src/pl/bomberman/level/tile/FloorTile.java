/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.level.tile;

import java.io.Serializable;
import pl.bomberman.graphics.Sprite;

/**
 *
 * @author MychauU
 */
public class FloorTile extends Tile implements Serializable{
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    public FloorTile(Sprite sprite) {
        super(sprite);
    }

    @Override
    public boolean isSolid() {
        return false;
    }

    

    @Override
    public boolean isBreakable() {
        return false;
    }

    
}
