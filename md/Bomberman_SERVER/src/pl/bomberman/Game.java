/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman;

import java.util.Iterator;
import pl.bomberman.level.Level;
import java.util.logging.Logger;
import pl.bomberman.entity.mob.Player;
import static pl.bomberman.level.Level.players;
import pl.bomberman.sendpack.MESSAGETYPE;
import pl.bomberman.sendpack.MessagePackage;
import pl.bomberman.server.Server;

/**
 *
 * klasa ktora przechowuje rozmiar okna
 * ograniczniki czasowe  dla update (ile razy update sie wykona na sekunde
 * 
 */
public class Game implements Runnable {

    public static final String title = "BomberMan";
    //skalowanie gry (czyli mozna zmniejszyc rozmiar okna i renderowanych pikseli)
    public static final double scale = 1;
    public static final int width = 1080;
    public static final int height = (width / 16) * 9;
    public static final int informationWidth = (int) ((int) 32 * 7);
    public static final int gameWidth = width - 56 - informationWidth;

    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    private boolean running = false;
    private Thread thread;
    private Level level;
    private Server server;
    private int numberOfPlayers;
    private boolean gameEnded = false;

    public boolean isGameEnded() {
        return gameEnded;
    }

    public void setGameEnded(boolean gameEnded) {
        this.gameEnded = gameEnded;
    }

    public Game(Server server) {
        this.server = server;
    }

    public Level getLevel() {
        return level;

    }

    public void setNewLevel() {
        level = new Level("/textures/levels/Level" + 1 + ".png");
    }

    //funkcja tworzaca nowy watek przebiegu gry
    public synchronized void start() {

        running = true;
        thread = new Thread(this, "game_app");
        thread.start();
    }

     //metoda konczaca wykonanie operacji run
    public synchronized void stop() {
        running = false;
        try {
            if (thread != null) {
                thread.join();
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    //glowna funkcja dzialajaca podczas grania
    @Override
    public void run() {
        long lastTime = System.nanoTime(); //czas w ns
        long timer = System.currentTimeMillis(); //czas w ms
        final double ns = 1000000000.0 / 60.0; //czas na wykonanie jednego frame w nanosekundach przy 60fps
        double delta = 0;
        int frames = 0;  // przechowuje ilosc przetworzonych klatek w render (to nie znaczy ze wszystkie sa zapisane (triple buffer)
        int updates = 0; //przechowuje ile razy updateowalismy gre (powinno byc  ~60fps zgodnie z delta
        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns; //jezeli roznica czasu pomiedz renderowanie a update jest wieksza niz jeden frame
            lastTime = now;
            while (delta >= 1) {
                update();
                updates++;
                delta--;
            }

            frames++;
            if (System.currentTimeMillis() - timer > 1000) { // raz na sekunde
                timer += 1000;
                //    System.out.println(title + " | " + updates + " ups, " + frames + " fps" + " | ");
                updates = 0;
                frames = 0;
            }
        }
    }

    //max 60 ups sprawdzania czy juz jakis gracz wygral , updateowanie wszystkich obiektow, wysylanie nowych danych lub ich odebranie
    public void update() {
        level.update();
        server.getMessageHandling().handleNewData();
        checkWinner();

    }

    public void setNumberOfPlayers(int size) {
        this.numberOfPlayers = size;
    }

    
    //funkcja sprawdzajaca czy jakis gracz juz wygral gre
    private void checkWinner() {
        if (!this.gameEnded) {
            int j = 0;
            Player winner = null;
            synchronized (Level.players) {
                Iterator i = players.iterator(); // Must be in synchronized block
                while (i.hasNext()) {
                    Player pom = (Player) i.next();
                    if (!pom.isRemoved()) {
                        winner = pom;
                        j++;
                    }
                }
                if (j == 1 && Level.players.size()>1) {
                    this.gameEnded = true;
                    if (winner != null) {
                        MessagePackage obj = new MessagePackage(null, MESSAGETYPE.WINNERPLAYERFROMSERVER, winner.getId());
                        Server.messages.add(obj);
                    }else{
                        MessagePackage obj = new MessagePackage(null, MESSAGETYPE.WINNERPLAYERFROMSERVER, 999);
                        Server.messages.add(obj);
                    }
                } else if (j == 0 && Level.players.size()>1) {
                    this.gameEnded = true;
                    MessagePackage obj = new MessagePackage(null, MESSAGETYPE.WINNERPLAYERFROMSERVER, 999);
                    Server.messages.add(obj);
                }
            }

        }
    }

}
