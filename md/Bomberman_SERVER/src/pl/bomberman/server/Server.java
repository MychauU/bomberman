/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.server;

import pl.bomberman.sendpack.MessagePackage;
import pl.bomberman.sendpack.MESSAGETYPE;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;
import pl.bomberman.Game;
import pl.bomberman.entity.mob.Player;
import pl.bomberman.level.Level;

/**
 *
 * @author MychauU
 */
public class Server {

    public static ArrayList<ConnectionToClient> clientList;
    public static LinkedBlockingQueue<Object> messages;
    public static List<Object> messagesToSent;
    protected ServerSocket serverSocket;
    protected MessageHandlingServerSide messageHandling;

    public MessageHandlingServerSide getMessageHandling() {
        return messageHandling;
    }
    protected  Thread msg;
    protected  Game game;
    private ConnectionRecievier connectionRecievier;
    private boolean startedRoomGame=false;
    
    static {
        clientList = new ArrayList<ConnectionToClient>();
        messages = new LinkedBlockingQueue<>();
        messagesToSent = new ArrayList<>();
    }
    
    
    public Server(int port) throws IOException {
        
        serverSocket = new ServerSocket(port);
        serverSocket.setPerformancePreferences(1, 2, 1);
        game = new Game(this);
        connectionRecievier=new ConnectionRecievier(this);
        Thread accept = new Thread(connectionRecievier, "thread save client to list");
        //accept.setDaemon(true);
        accept.start();

        messageHandling = new MessageHandlingServerSide(this);
        msg = new Thread(messageHandling, "messageHandlingToServerFromQueue");
        msg.start();

    }

    //wyslij dane do jednego klienta
    public void sendToOne(int index, Object message) throws IndexOutOfBoundsException {
        clientList.get(index).write(message);
    }

    //wyslij dane do wszystkich klientow
    public void sendToAll(Object message) {
        for (ConnectionToClient pom : clientList) {
            pom.write(message);

        }
    }

    public void sendToOneWithoutFlush(int index, Object message) throws IndexOutOfBoundsException {
        clientList.get(index).writeWithoutFlush(message);
    }

    //wyslij dane do wszystkich klientow
    public void sendToAllWithoutFlush(Object message) {
        for (ConnectionToClient pom : clientList) {
            pom.writeWithoutFlush(message);

        }
    }

    
    //funkcja do resetowania listy klientow  gdy klienci wyszli
    protected void checkGame() {
        if (clientList.isEmpty()) {
            try {
                this.startedRoomGame=false;
                if (this.messageHandling!=null){
                    this.messageHandling.setRunning(false);
                }
                if (this.msg!=null){
                msg.join(1000);
                }
                if (this.game!=null){
                    this.game.stop();
                }
                this.game = new Game(this);
                Level.clearLevelElements();
                Server.clientList.clear();
                Server.messages.clear();
                Server.messagesToSent.clear();
                //Level.
                msg = new Thread(messageHandling, "messageHandlingToServerFromQueue");
                msg.start();
            } catch (InterruptedException ex) {
                Logger.getLogger(Server.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }

        }
    }

    

    public synchronized void setStarted() {
        game.setNewLevel();
        game.setGameEnded(false);
        game.setNumberOfPlayers(clientList.size());
        //pozniej zmienie
        for (int i = 0; i < clientList.size(); i++) {
            Player pom = new Player(0, 0, i, game.getLevel());
            switch (i) {
                case 0:
                    pom.x = 48;
                    pom.y = 48;
                    break;
                case 1:
                    pom.x = 754;
                    pom.y = 48;
                    break;
                case 2:
                    pom.x = 48;
                    pom.y = 528;
                    break;
                case 3:
                    pom.x = 754;
                    pom.y = 528;
                    break;
                default:
                    break;
            }
            Level.players.add(pom);
        }
        MessagePackage msg = new MessagePackage();
        msg.message = MESSAGETYPE.SENDLEVEL;
        msg.obj = game.getLevel();
        sendToAll(msg);

        msg = new MessagePackage();
        msg.message = MESSAGETYPE.SENDPLAYERSFROMSERVER;
        msg.obj = Level.players;
        sendToAll(msg);

        msg = new MessagePackage();
        msg.message = MESSAGETYPE.SENDBOXESFROMSERVER;
        msg.obj = Level.boxes;
        sendToAll(msg);

        String tab[] = new String[Server.clientList.size()];
        int i = 0;
        synchronized (Server.clientList) {
            for (ConnectionToClient pom : Server.clientList) {
                tab[i++] = pom.name;
            }
        }
        msg = new MessagePackage();
        msg.message = MESSAGETYPE.SENDPLAYERSNAMEFROMSERVER;
        msg.obj = tab;
        sendToAll(msg);

        msg = new MessagePackage();
        msg.message = MESSAGETYPE.STARTGAME;
        sendToAll(msg);
        messageHandling.setRunning(false);
        this.startedRoomGame=true;
        game.start();
    }
    
    
    private class ConnectionRecievier implements Runnable {

        Server parent;

        public ConnectionRecievier(Server server) {
            this.parent = server;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    // InetSocketAddress wtf=serverSocket.getLocalSocketAddress();
                    Socket s = serverSocket.accept();
                    if (clientList.size() >= 4 || startedRoomGame) {                        
                        //tutaj odpowiedz serwera ze gra wystartowala lub ze juz jest za duzo graczy
                        s.close();
                        continue;
                    }
                    ConnectionToClient connectionToClient = new ConnectionToClient(s, this.parent);
                    clientList.add(connectionToClient);
                    connectionToClient.id = clientList.indexOf(connectionToClient);
                    MessagePackage pom = new MessagePackage(null, MESSAGETYPE.IDENTIFY, clientList.indexOf(connectionToClient));
                    pom.additionalInformation = 0;
                    sendToOne(clientList.indexOf(connectionToClient), pom);
                    System.out.println(s.getRemoteSocketAddress().toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
