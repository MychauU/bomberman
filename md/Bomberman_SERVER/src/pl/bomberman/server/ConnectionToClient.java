/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.bomberman.sendpack.MESSAGETYPE;
import pl.bomberman.sendpack.MessagePackage;

/**
 *
 * @author MychauU
 */
public class ConnectionToClient {

    Server parent;
    Socket socket;
    public ObjectInputStream in;
    public ObjectOutputStream out;
    public String name;
    public int id;
    private RecievierOfMessages recieveObj;

    public ConnectionToClient(Socket x, Server parent) throws IOException {
        this.parent = parent;
        socket = x;
        socket.setTcpNoDelay(true);
        out = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
        out.flush();
        in = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
        recieveObj = new RecievierOfMessages(this);

        Thread read = new Thread(recieveObj, "thread get data from client");

        //  read.setDaemon(true); // terminate when main ends
        read.start();
    }

    //wyslij dane do klienta
    public void write(Object obj) {
        try {
            out.writeObject(obj);

            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeWithoutFlush(Object obj) {
        try {
            out.writeObject(obj);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class RecievierOfMessages implements Runnable {

        ConnectionToClient parent;

        public RecievierOfMessages(ConnectionToClient parent) {
            this.parent = parent;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    Object obj = in.readObject();
                    if (obj != null) {
                        Server.messages.put(obj);
                    }

                } catch (IOException | ClassNotFoundException | InterruptedException ex) {
                    Logger.getLogger(ConnectionToClient.class.getName()).log(Level.SEVERE, null, ex);
                    synchronized (Server.clientList) {
                        Iterator x = Server.clientList.iterator();
                        while (x.hasNext()) {
                            ConnectionToClient pom = (ConnectionToClient) x.next();
                            if (pom == this.parent) {
                                try {
                                    this.parent.out.close();
                                    this.parent.in.close();
                                    this.parent.socket.close();
                                    MessagePackage  obj = new MessagePackage(null, MESSAGETYPE.DISCONNECTEDPLAYERFROMSERVER, pom.id );
                                    Server.messages.add(obj);
                                    x.remove();
                                    break;
                                } catch (IOException ex1) {
                                    Logger.getLogger(ConnectionToClient.class.getName()).log(Level.SEVERE, null, ex1);
                                }
                            }
                        }
                    }
                    this.parent.parent.checkGame();
                    break;
                }

            }
        }
    }

}
