/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.server;

import java.awt.Point;
import static pl.bomberman.server.Server.messages;
import pl.bomberman.level.Level;
import pl.bomberman.sendpack.MESSAGETYPE;
import pl.bomberman.sendpack.MessagePackage;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Logger;
import pl.bomberman.StaticFunctions;
import pl.bomberman.entity.Bomb;

/**
 *
 * @author MychauU
 */
public class MessageHandlingServerSide implements Runnable {

    private Server server;
    private boolean running;

    public MessageHandlingServerSide(Server server) {
        this.server = server;
    }

    public synchronized void setRunning(boolean running) {
        this.running = running;
    }

    @Override
    public void run() {
        running = true;
        while (running) {
            try {
                Object message = messages.take();
                if (message instanceof MessagePackage) {
                    MessagePackage obj = (MessagePackage) message;
                    if (null != obj.message) {
                        switch (obj.message) {
                            case SENDPLAYERNAMEFROMCLIENT: {
                                synchronized (Server.clientList) {
                                    for (ConnectionToClient pom : Server.clientList) {
                                        if (pom.id==obj.id) {
                                            pom.name = (String) obj.obj;
                                        }
                                    }
                                }

                            }
                            break;
                            case CREATENEWGAME: {
                                this.server.setStarted();
                            }
                            break;
                            default:
                                break;
                        }
                    }
                }
                // Do some handling here...

            } catch (InterruptedException e) {
            }
        }
    }

    public void handleNewData() {
        while (true) {
            Object message = messages.poll();
            if (message == null) {
                break;
            } else if (message instanceof MessagePackage) {
                MessagePackage obj = (MessagePackage) message;
                if (null != obj.message) {
                    switch (obj.message) {
                        case MOVEMENTFROMCLIENT: {
                            Point xy = (Point) obj.obj;
                            //    Point2D.Double oldXY=Level.players.get(obj.id).getCooridinates();
                            Level.players.get(obj.id).move(xy.x, xy.y);
                            Point2D.Double newXY = Level.players.get(obj.id).getCooridinates();
                            obj = new MessagePackage(newXY, MESSAGETYPE.MOVEMENTFROMSERVER, obj.id);
                            obj.additionalInformation = Level.players.get(obj.id).setDirection(xy.x, xy.y);
                            Server.messagesToSent.add(obj);
                            //this.server.sendToAll(obj);

                        }
                        break;
                        case CREATEBOMBFROMCLIENT: {
                            Point2D.Double xy = (Point2D.Double) obj.obj;
                            boolean collision = false;
                            synchronized (Level.bombs) {
                                Iterator i = Level.bombs.iterator(); // Must be in synchronized block
                                while (i.hasNext()) {
                                    Bomb pom = (Bomb) i.next();
                                    if (StaticFunctions.collision(pom.x, pom.y, xy.x, xy.y)) {
                                        collision = true;
                                        break;
                                    }
                                }
                            }
                            if (!collision) {
                                int bombRange = Level.players.get(obj.id).getBombRange();
                                if (Level.players.get(obj.id).getBombsInHand() >= 1) {
                                    Level.players.get(obj.id).decBombsInHand();
                                    Level.bombs.add(new Bomb(xy.x, xy.y, this.server.game.getLevel(), bombRange, obj.id));
                                    obj = new MessagePackage(xy, MESSAGETYPE.CREATEBOMBFROMSERVER, obj.id);
                                    obj.additionalInformation = bombRange;
                                    Server.messagesToSent.add(obj);
                                    //this.server.sendToAll(obj);
                                }
                            }
                        }
                        break;
                        case DELETEBOMBFROMSERVER: {
                            Server.messagesToSent.add(obj);
                            //    this.server.sendToAll(obj);
                        }
                        break;
                        case CREATEFIREFROMSERVER: {
                            Server.messagesToSent.add(obj);
                            //    this.server.sendToAll(obj);
                        }
                        break;
                        case REMOVEBOXFROMSERVER: {
                            Server.messagesToSent.add(obj);
                            //    this.server.sendToAll(obj);
                        }
                        break;
                        case CREATEPOWERUPFROMSERVER: {
                            Server.messagesToSent.add(obj);
                            //  this.server.sendToAll(obj);
                        }
                        break;
                        case REMOVEPOWERUPFROMSERVER: {
                            //     this.server.sendToAll(obj);
                            Server.messagesToSent.add(obj);
                        }
                        break;
                        case DECLIFE: {
                            //   this.server.sendToAll(obj);
                            Server.messagesToSent.add(obj);
                        }
                        case DISCONNECTEDPLAYERFROMSERVER: {
                            //   this.server.sendToAll(obj);
                            Server.messagesToSent.add(obj);
                        }
                        break;
                        case WINNERPLAYERFROMSERVER: {
                            //   this.server.sendToAll(obj);
                            Server.messagesToSent.add(obj);
                        }
                        break;
                        default:
                            break;
                    }
                }
            }
            // Do some handling here...

        }
        for (Iterator<Object> it = Server.messagesToSent.iterator(); it.hasNext();) {
            Object pom = it.next();
            server.sendToAllWithoutFlush(pom);

        }
        Server.messagesToSent.clear();
        for (ConnectionToClient pom : Server.clientList) {
            try {
                pom.out.flush();
            } catch (IOException ex) {
                Logger.getLogger(MessageHandlingServerSide.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
    }

}
