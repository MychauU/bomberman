/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman;

import java.awt.Point;
import java.io.Serializable;
import pl.bomberman.graphics.Sprite;

/**
 *
 * @author MychauU
 */
public class StaticFunctions implements Serializable {

    public static double triangleArea(Point A, Point B, Point C) {
        return (C.x * B.y - B.x * C.y) - (C.x * A.y - A.x * C.y) + (B.x * A.y - A.x * B.y);
    }

    public static boolean isInsideSquare(Point A, Point B, Point C, Point D, Point P) {
        return !(triangleArea(A, B, P) > 0 || triangleArea(B, C, P) > 0 || triangleArea(C, D, P) > 0 || triangleArea(D, A, P) > 0);
    }

    public static boolean collisionFourPoint(double xa, double ya, double x, double y) {
        int xt, yt;
        Point P = new Point();
        Point xy1 = new Point();
        Point xy2 = new Point();
        Point xy3 = new Point();
        Point xy4 = new Point();
        double xx, yy;
        int ix, iy;
        for (int c = 0; c < 4; c++) {
            xx = ((xa) + c % 2 * (Sprite.void_sprite.SIZE_BLOCK >> 1) - 8);
            yy = ((ya) + c / 2 * (Sprite.void_sprite.SIZE_BLOCK >> 1) - 4);
            ix = (int) Math.ceil(xx);
            iy = (int) Math.ceil(yy);
            if (c % 2 == 0) {
                ix = (int) Math.floor(xx);
            }
            if (c / 2 == 0) {
                iy = (int) Math.floor(yy);
            }
            P.x = ix;
            P.y = iy;
            xt = (int) x;
            yt = (int) y;
            xy1.x = xt - (Sprite.void_sprite.SIZE_BLOCK >> 1) + 1;
            xy1.y = yt - (Sprite.void_sprite.SIZE_BLOCK >> 1) + 1;
            xy2.x = xt + (Sprite.void_sprite.SIZE_BLOCK >> 1) - 1;
            xy2.y = yt - (Sprite.void_sprite.SIZE_BLOCK >> 1) + 1;
            xy3.x = xt + (Sprite.void_sprite.SIZE_BLOCK >> 1) - 1;
            xy3.y = yt + (Sprite.void_sprite.SIZE_BLOCK >> 1) - 1;
            xy4.x = xt - (Sprite.void_sprite.SIZE_BLOCK >> 1) + 1;
            xy4.y = yt + (Sprite.void_sprite.SIZE_BLOCK >> 1) - 1;
            if (isInsideSquare(xy1, xy2, xy3, xy4, P)) {
                return true;
            }
        }
        return false;
    }

    public static boolean collision(double xa, double ya, double x, double y) {
        int xt, yt;
        Point P = new Point();
        Point xy1 = new Point();
        Point xy2 = new Point();
        Point xy3 = new Point();
        Point xy4 = new Point();
        double xx, yy;
        int ix, iy;
        P.x = (int) xa;
        P.y = (int) ya;
        xt = (int) x;
        yt = (int) y;
        xy1.x = xt - (Sprite.void_sprite.SIZE_BLOCK >> 1) + 1;
        xy1.y = yt - (Sprite.void_sprite.SIZE_BLOCK >> 1) + 1;
        xy2.x = xt + (Sprite.void_sprite.SIZE_BLOCK >> 1) - 1;
        xy2.y = yt - (Sprite.void_sprite.SIZE_BLOCK >> 1) + 1;
        xy3.x = xt + (Sprite.void_sprite.SIZE_BLOCK >> 1) - 1;
        xy3.y = yt + (Sprite.void_sprite.SIZE_BLOCK >> 1) - 1;
        xy4.x = xt - (Sprite.void_sprite.SIZE_BLOCK >> 1) + 1;
        xy4.y = yt + (Sprite.void_sprite.SIZE_BLOCK >> 1) - 1;
        return isInsideSquare(xy1, xy2, xy3, xy4, P);
    }
}
