/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bomberman.graphics;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import pl.bomberman.Main;

/**
 *
 * @author MychauU
 */
public class SpriteSheet implements Serializable{
    private static final long serialVersionUID = 1L; //http://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html
    private final String path;
    public final int SIZE;
    public int[] pixels;
    public final int SIZE_BLOCK;
    
    public static SpriteSheet tiles = new SpriteSheet(640, "/textures/sheets/spritesheet32.png", 32);

    public SpriteSheet(int size, String path, int blockSize) {
        this.path = path;
        this.SIZE = size;
        this.SIZE_BLOCK = blockSize;
        pixels = new int[SIZE * SIZE];
        load();
    }

    private void load() {
        try {
            BufferedImage image = ImageIO.read(SpriteSheet.class.getResourceAsStream(path));
            int w = image.getWidth();
            int h = image.getHeight();
            //   BufferedImage image2= new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);   
            //    image2=ImageIO.read(SpriteSheet.class.getResource(path));
            //    image2.getRGB(0, 0, w, h, pixels, 0, w);
            image.getRGB(0, 0, w, h, pixels, 0, w);
        } catch (IOException ex) {
            Logger.getLogger(SpriteSheet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*public static BufferedImage resize(BufferedImage img, int newW, int newH) {
        Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return dimg;
    }*/
}
